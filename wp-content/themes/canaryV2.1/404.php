<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */

get_header(); ?>
<body>
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
				<h1><?php _e( 'We can\'t find the page you\'re looking for!', 'twentyten' ); ?></h1>
				
				<p><?php _e( 'Apologies, but the page you requested could not be found.', 'twentyten' ); ?></p>
				
				<?php //get_search_form(); ?>
</div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('can-footer.php'); 
	?>
    
			
</body>

</html>