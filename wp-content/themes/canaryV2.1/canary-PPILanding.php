<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: PPI Landing page
 */

get_header(); ?>

<body id="payment">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
        <h1>Thank You For Your Request!</h1>
        <div>
		<div class="col1">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content(); ?>

        </div>
    </div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
    </div>
    <div class="col2">
    	<div id="body-yellow">
            <img src="<?php bloginfo('url'); ?>/assets/what-should.png" alt="What should I do next?" class="img-yellow-ppi"/>
            <a href="<?php bloginfo('url'); ?>/contact-us/?from=ppi" class="yellow-opt1"><span class="alt">Option 1 | Contact us for a claims pack</span></a>
            <a href="<?php bloginfo('url'); ?>/pdf/PPI_Reclaim.pdf" class="yellow-opt2"><span class="alt">Option 2 | Download the forms now</span></a>
        </div>
    </div>
    </div>
		<div class="content-bottom"></div>
	</div>			
	<?php 
		require('can-footer.php'); 
	?>
</body>

</html>