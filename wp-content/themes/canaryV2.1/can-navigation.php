<?php
/*
*
*
*/
$liveHost = true;
if ($_SERVER['HTTP_HOST'] == "localhost") $liveHost = false;
?>

<div class="main">
	<div class="header">
    	<div class="header-links"><span class="h-link-txt"><a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/privacy/"; else echo "?page_id=348" ?>" title="Privacy" class="h-links">Privacy</a> | <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/terms-and-conditions/"; else echo "?page_id=51" ?>" title="Terms &amp; Conditions" class="h-links">Terms &amp; Conditions</a> | <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/contact-us/"; else echo "?page_id=9" ?>" title="Contact" class="h-links" rel="nofollow">Contact</a></span></div>
    	<div class="header-middle-left">
        	<img src="<?php bloginfo('url'); ?>/assets/canary-claims-logo.png" alt="Canary Claims logo" class="main-logo" />
        </div>
        <div class="header-middle-right">
        	<img src="<?php bloginfo('url'); ?>/assets/canary-claims-phone01.png" alt="0800 634 8668" class="telephone-number" />
            <img src="<?php bloginfo('url'); ?>/assets/canary-claims-phone02.png" alt="0208 269 2291" class="telephone-number" />
        </div>
    </div>
    <div class="navigation">
    	<ul id="nav">
            <li><a class="home-nav" href="<?php bloginfo('url'); ?>"><span class="alt">Home</span></a></li>
            <li><a class="about-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/about-us/"; else echo "?page_id=2" ?>"><span class="alt">About</span></a></li>
            <li><a class="credit-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/credit-card-charges/"; else echo "?page_id=102" ?>"><span class="alt">Credit Card Charges</span></a></li>
            <li><a class="payment-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/payment-protection/"; else echo "?page_id=5" ?>"><span class="alt">Payment Protection</span></a></li>
            <li><a class="win-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/no-win-no-fee/"; else echo "?page_id=6" ?>"><span class="alt">No Win No Fee</span></a></li>
            <li><a class="refer-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/refer-a-friend/"; else echo "?page_id=7" ?>"><span class="alt">Refer a Friend</span></a></li>
            <li><a class="faq-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/faqs/"; else echo "?page_id=8" ?>"><span class="alt">FAQ</span></a></li>
            <li><a class="ontv-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/as-seen-on-tv/"; else echo "?page_id=3" ?>"><span class="alt">As Seen on TV</span></a></li>
            <li><a class="contact-nav" href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/contact-us/"; else echo "?page_id=9" ?>"><span class="alt">Contact</span></a></li>
        </ul>
    </div>