<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Contact page
 */
/*$flag = false;
if($_POST['sbtbtn'])
{
	$details = '';
	if($_POST['numofBC'] != 'notset')
		$details .= 'Number of Loans : '.$_POST['numofBC']."\n";

	if($_POST['numofCC'] != 'notset')
		$details .= 'Number of Credit Cards : '.$_POST['numofCC']."\n";

	if($_POST['numofPPI']  != 'notset')
		$details .= 'Number of Policies : '.$_POST['numofPPI']."\n";

	if($_POST['name'])
		$name = $_POST['name'];

	if($_POST['email'])
		$email = $_POST['email'];

	if($_POST['address'])
		$address = $_POST['address'];

	if($_POST['tel'])
		$tel = $_POST['tel'];

		$message = "Hi,\n\nYou have recieved a mail from the web form\n\nName : $name\nEmail : $email\nPhone : $tel\nAddress : $address\n";

		$to      = 'steve@seo-creative.co.uk';
		$subject = 'Mail from web form';
		$headers = 'From: noreply@canaryclaims.co.uk' . "\r\n" .
		'Reply-To: noreply@canaryclaims.co.uk' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		if(mail($to, $subject, $message.$details."\n\nThank you\nWebmaster", $headers))
		$flag = true;

}*/
$from = $_GET["from"];
get_header(); ?>

<body id="contact">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        	<div>
            	<h1>Contact Us</h1>
                
                
                <div class="col1">
                 <div id="contact-bg-outer">
		  <div id="contact-bg-inner">
					
		  
                    
                    
                    <form name="contact" method="post" id="contactForm" action="<?php bloginfo('url'); ?>/form-sent/" onSubmit="return doValidation(this);">
                    
                    <?php $htmlCCnumber = "<span class='askNumber' style='display:none'><br />Number of Credit Cards: <select name='numofCC' id='numofCC'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    //$htmlBCnumber = "<span class='askNumber' style='display:none'><br />Number of Loans: <select name='numofBC' id='numofBC'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    $htmlPPInumber = "<span class='askNumber' style='display:none'><br />Number of Policies: <select name='numofPPI' id='numofPPI'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    
                    $htmlCCnumberDefault = "<span class='askNumber' style='display:inline'><br />Number of Credit Cards: <select name='numofCC' id='numofCC'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    //$htmlBCnumberDefault = "<span class='askNumber' style='display:inline'><br />Number of Loans: <select name='numofBC' id='numofBC'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    $htmlPPInumberDefault = "<span class='askNumber' style='display:inline'><br />Number of Policies: <select name='numofPPI' id='numofPPI'><option value='notset'>- please select -</option><option value='01'>- 1 -</option><option value='02'>- 2 -</option><option value='03'>- 3 -</option><option value='04'>- 4 -</option><option value='05'>- 5 -</option><option value='06'>- 6 -</option><option value='07'>- 7 -</option><option value='08'>- 8 -</option><option value='09'>- 9 -</option><option value='10'>- 10 -</option></select></span>";
                    
                    
                    switch ($from){
                    
                    case "ccc":
                    print "<div id=\"appliersOnly\">";
                    print  "<H5>You would like to reclaim:</H5>";
                    print "<p class=\"Text MoreMargin\"><input  onclick='toggleNumSelect(this)' name=\"alsoppi\"class=\"applyAlsoChbx\" type='checkbox' value='ppi' /> <strong>Payment Protection Insurance (PPI)</strong>" . $htmlPPInumber . "</p>";
                    //print "<p class=\"Text\"><input   onclick='toggleNumSelect(this)' name=\"alsobc\"class=\"applyAlsoChbx\" type='checkbox' value='bc' /> <strong>Loans</strong>" . $htmlBCnumber . "</p>";
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)' name=\"mainApplyItem\" checked='checked' class=\"applyAlsoChbx\" type='checkbox' value='Credit Card Charges' /> <strong>Credit Card Charges</strong>" . $htmlCCnumberDefault . "</p>";
                    print "</div>" ;
                    break;
                    case "loans":
                    print "<div id=\"appliersOnly\">";
                    print "<H5 class=\"Text\">You would like to reclaim:</strong></H5>";
                    print "<p class=\"Text MoreMargin\"><input   onclick='toggleNumSelect(this)' name=\"alsoppi\"class=\"applyAlsoChbx\" type='checkbox' value='ppi' /> <strong>Payment Protection Insurance (PPI)</strong>" . $htmlPPInumber . "</p>";
                    //print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)' checked='checked' name=\"mainApplyItem\" class=\"applyAlsoChbx\" type='checkbox' value='Loans' /> <strong>Loans</strong>" . $htmlBCnumberDefault . "</p>";
                    
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)'  name=\"alsoccc\"class=\"applyAlsoChbx\" type='checkbox' value='ccc' /> <strong>Credit Card Charges</strong>" . $htmlCCnumber . "</p>";
                    print("</div>");
                    break;
                    case "ppi":
                    print "<div id=\"appliersOnly\">";
                    print  "<H5 class=\"Text\">You would like to reclaim:</H5>";
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)' checked='checked'  name=\"mainApplyItem\" class=\"applyAlsoChbx\" type='checkbox' value='Payment Protection' /> <strong>Payment Protection Insurance (PPI)</strong>" . $htmlPPInumberDefault . "</p>";
                    //print "<p class=\"Text MoreMargin\"><input  onclick='toggleNumSelect(this)'  name=\"alsobc\"class=\"applyAlsoChbx\" type='checkbox' value='bc' /> <strong>Loans</strong>" . $htmlBCnumber . "</p>";
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)'  name=\"alsoccc\"class=\"applyAlsoChbx\" type='checkbox' value='ccc' /> <strong>Credit Card Charges</strong>" . $htmlCCnumber . "</p>";
                    print "</div>";
                    break;
                    default:
                    print "<div id=\"appliersOnly\">";
                    print  "<H5 class=\"Text\"><strong>You would like to reclaim:".$from."</strong></H5>";
                    print "<input name=\"applyNoSelected\" type='hidden' value='true' />";
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)'  name=\"appppi\"class=\"applyAlsoChbx\" type='checkbox' value='true' /> <strong>Payment Protection Insurance (PPI)</strong>" . $htmlPPInumber . "</p>";
                    //print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)' name=\"appbc\" class=\"applyAlsoChbx\" type='checkbox' value='true' /> <strong>Loans</strong>" . $htmlBCnumber . "</p>";
                    print "<p class=\"Text\"><input  onclick='toggleNumSelect(this)'  name=\"appccc\"class=\"applyAlsoChbx\" type='checkbox' value='true' /> <strong>Credit Card Charges</strong>" . $htmlCCnumber . "</p>";
                    
                    print "</div>";
                    break;
                    }
					
					?>
                    
                    <h5>Contact us to receive a claims pack</h5>
                    <p><b>Title</b><select id="selectTitleForContact" name="title" id="title"><option value="Mr" selected>Mr</option><option value="Mrs">Mrs</option><option value="Miss">Miss</option><option value="Ms">Ms</option></select></p>
                    <p><b>Name*</b><input type="text" id="name" name="username" class="textfild" />
                    </p>
                    <p><b>Email</b><input type="text" class="textfild" id="email" name="email" /></p>
                    <p><b>Tel*</b><input type="text" class="textfild" id="tel" name="tel" /></p>
                    <p><b>Address*</b><textarea id="adtest" name="adtest" /></textarea></p>
                    <p>
                    <input name="sbtbtn" type="submit" value="Submit" class="submitt02"  />
                    <input type="reset" value="Reset"  class="submitt04"/>
                    <a href="<?php echo get_permalink(48); ?>" title="Privacy Policy" rel="nofollow" >Privacy Policy</a>
                    </p>
                    </form>
                    </div></div>
                    
                </div>
                
                
                <div class="col2">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
				</div>
        	</div>
            <div class="contact-downloads"><p><strong><span style="color:#4b8a35;">Download Application Forms:</span></strong></p>
            <p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/PPI_Reclaim.pdf" title="PPI_Reclaim.pdf">Application form 1 - Payment Protection Insurance</a></p>
            <p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/Credit_Card_Charge_Claim.pdf" title="Credit_Card_Charge_Claim.pdf">Application form 2 - Credit Card Charges</a></p>
            </div>
    	</div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
    
			</div>
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
</body>
<script language="javascript">
function toggleNumSelect(elemDom){
if(elemDom.checked) {elemDom.parentNode.getElementsByTagName("span")[0].style.display = "inline";}
else {elemDom.parentNode.getElementsByTagName("span")[0].style.display = "none";}
}
</script>
<script language="javascript">

	function doValidation(form) {
		var proceed = true; // Initially assume that validation is successful.
		var errorText = "Errors were found in your form. Please amend:\n\n";
		var emailCheck = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,4})$/;


		if (trim(form.name.value) == '') {
			errorText += "- Please enter your full name\n";
			proceed = false;
		}

		if (trim(form.tel.value) == '') {
			errorText += "- Please enter your telephone number\n";
			proceed = false;
		}
		
		if (trim(form.adtest.value) == '') {
			errorText += "- Please enter your address\n";
			proceed = false;
		}
		
		if (!proceed){ 
			alert(errorText); 
		}
		
		
		return proceed;
	}

	function trim(toTrim) {
		while(''+toTrim.charAt(0) == " ") { toTrim = toTrim.substring(1,toTrim.length); }
		while(''+toTrim.charAt(toTrim.length-1) == " ") { toTrim = toTrim.substring(0,toTrim.length-1); }
		return toTrim;
	}
</script>
</html>