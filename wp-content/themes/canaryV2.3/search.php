<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */
include('quickclaim.php');
get_header(); ?>
<body>
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
<?php if ( have_posts() ) : ?>
				<h1><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h1>
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				// get_template_part( 'loop', 'search' );
				
    while (have_posts()) : the_post(); ?>
    <div class="blog-summary-wrapper">
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        
         
 
        <div class="entry-sum">
        <p class="blogheading"><a href="<?php the_permalink() ?>" class="blogheading" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
        <?php the_excerpt(); ?>
        </div>
		
        <p class="postmetadata-right">Posted in <?php the_category(', ') ?> | By <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="nofollow">
									<?php printf( __('%s'),get_the_author());?></a> | <?php the_time('F jS, Y') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></p>
                                    
       
       
        <br />
    </div>
    </div>
    <div class="divide-horizontal"></div>
	<?php endwhile; ?>
		
    <div class="blognavigationbottom">
        	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php //get_search_form(); ?>

	<?php endif; ?>
</div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('can-footer.php'); 
	?>
    
			
</body>

</html>