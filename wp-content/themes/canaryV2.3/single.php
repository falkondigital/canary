<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */
include('quickclaim.php');
get_header(); ?>
<body id="news">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
                <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    <h1><?php the_title(); ?></h1>
                  <div class="small">Posted on <?php the_time('F jS Y') ?> by <?php printf( __('%s'),get_the_author());?></div>
        
                    <div class="entry">
                        <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
        
                        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                        
                     <div class="divide-horizontal"></div>  
        <p><strong>Author:</strong> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __('%s'),get_the_author());?></a> 
        <br/><strong>Posted in: </strong> <?php the_category(', '); ?>
        <br/><?php the_tags('<strong>Tags:</strong> ', ', ', '<br />'); ?>
        </p>
        <div class="divide-horizontal"></div> 
        
                    </div>
                </div>
                
                <div class="blognavigation">
                    <div class="alignleftbrac"><?php previous_post_link('&lt;') ?></div><div class="alignleftnav"><?php previous_post_link('%link') ?></div>
                    <div class="alignrightbrac"><?php next_post_link('&gt;') ?></div><div class="alignrightnav"><?php next_post_link('%link') ?></div>
                </div>
                <?php comments_template(); ?>
                <?php endwhile; else: ?>
                <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>
</div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('can-footer.php'); 
	?>			
</body>
