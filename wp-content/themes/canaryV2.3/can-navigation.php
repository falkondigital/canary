<?php
/*
*
*
*/
$liveHost = true;
if ($_SERVER['HTTP_HOST'] == "localhost") $liveHost = false;
?>    

<?php if( !isset( $_COOKIE['canaryclaims-cookie-app'] ) ) { ?>
<div id="header-cookie-bar">
	<div id="cookie-wrap">
    	<p> Like over 90% of all websites, we use cookies. They are used for the site functionality, as well as to evaluate & improve the site. </p>
        <a href="<?php echo get_permalink(447);?>"><div id="find-out-more"><span class="alt">Find out more</span></div></a>
		<div id="accept-cookies"><span class="alt">Accept</span></div>
    </div>
</div>
<?php  } ?>

<div class="header-wrap">
    <div class="header">
        <div class="header-links"><span class="h-link-txt"><a href="<?php echo get_permalink(48); ?>" title="Privacy" class="h-links">Privacy</a> | <a href="<?php echo get_permalink(51); ?>" title="Terms &amp; Conditions" class="h-links">Terms &amp; Conditions</a> | <a href="<?php echo get_permalink(9); ?>" title="Contact" class="h-links" rel="nofollow">Contact</a></span></div>
        <div class="header-middle-left">
            <a href="<?php echo home_url('/'); ?>"><img src="<?php bloginfo('url'); ?>/assets/canary-claims-logo.png" alt="Canary Claims logo" class="main-logo" /></a>
        </div>
        
        <div id="header-discount"><img src="<?php echo get_template_directory_uri(); ?>/images/header-discount.png" width="137" height="136" /></div>
        
        <div class="header-middle-right">
        	<span class="telephone-number">0800 634 8668</span>
            <span class="telephone-number">0208 269 2291</span>
            <!--<img src="<?php bloginfo('url'); ?>/assets/canary-claims-phone01.png" alt="0800 634 8668" class="telephone-number" />
            <img src="<?php bloginfo('url'); ?>/assets/canary-claims-phone02.png" alt="0208 269 2291" class="telephone-number" />-->
        </div>
    </div>
</div>
<div class="main">
    <div class="navigation">
    	<ul id="nav">
            <li><a class="home-nav" href="<?php echo get_permalink(3); ?>"><span class="alt">Home</span></a></li>
            <li><a class="about-nav" href="<?php echo get_permalink(2); ?>"><span class="alt">About</span></a></li>
            <?php /*?><li><a class="credit-nav" href="<?php echo get_permalink(102); ?>"><span class="alt">Credit Card Charges</span></a></li><?php */?>
            <li><a class="payment-nav" href="<?php echo get_permalink(5); ?>"><span class="alt">Payment Protection</span></a></li>
            <li><a class="win-nav" href="<?php echo get_permalink(6); ?>"><span class="alt">No Win No Fee</span></a></li>
            <li><a class="refer-nav" href="<?php echo get_permalink(7); ?>"><span class="alt">Refer a Friend</span></a></li>
            <li><a class="faq-nav" href="<?php echo get_permalink(8); ?>"><span class="alt">FAQ</span></a></li>
            <li><a class="ontv-nav" href="<?php echo get_permalink(248); ?>"><span class="alt">As Seen on TV</span></a></li>
            <li><a class="contact-nav" href="<?php echo get_permalink(9); ?>"><span class="alt">Contact</span></a></li>
        </ul>
</div>