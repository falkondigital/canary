<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Homepage
 */
include('quickclaim.php');
get_header(); ?>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url');?>/js/jquery.easing.1.2.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url');?>/js/jquery.anythingslider.js" /></script>
<!--<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<script type="text/javascript" src="js/jquery.simplyscroll-1.0.4.js"></script>
<script type="text/javascript">
(function($) {
	$(function() { //on DOM ready
		$("#scroller").simplyScroll({
			autoMode: 'loop',
			horizontal: false
		});
	});
})(jQuery);
</script>
<script src="js/more-show.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>
<!--<link rel="stylesheet" type="text/css" href="skin.css" />-->
<!--<script language="javascript" type="text/javascript">-->
<!--jQuery(document).ready(function() {-->
<!--	jQuery('div.more-text').hide();-->
<!--	jQuery('div.read-more-home').click(function() {-->
<!--		jQuery(this).toggleClass("active").prev().slideToggle('normal');-->
<!--	});-->
<!---->
<!--});-->
<!--</script>-->
<script type="text/javascript">
	   jQuery(document).ready(
				function(){
					jQuery('ul#portfolio').innerfade({
						speed: 1000,
						timeout: 4000,
						type: 'sequence',
						containerheight: '254px'
					});
			});
  	</script>

<body id="home" <?php body_class(); ?>>
	<?php
	  require('can-navigation.php');
	?>

     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        <div class="homepage-content-left">
        	<div class="calltoaction">
            <div class="cta-left">
            <ul id="portfolio">
					<?php /* ?>
                    <li>
						<div id="tile1">
                            <img src="<?php bloginfo('url'); ?>/assets/cta-tile2-title.png" alt="HAVE YOU BEEN MIS SOLD PPI?" />
                            <img src="<?php bloginfo('url'); ?>/assets/cta-tile2-question.png" alt="Image of a question mark" class="cta-img-left" />
                            <p class="cta-txt">Mis sold PPI (Payment Protection Insurance) is an insurance premium added to your credit card and loan repayments without your knowledge, full understanding, or if you were exempt from receiving the cover.</p>
                        </div>
					</li>
                    <li>
						<div id="tile2">
                        <img src="<?php bloginfo('url'); ?>/assets/cta-tile3-title.png" alt="CLAIM BACK YOUR PPI" />
                        <img src="<?php bloginfo('url'); ?>/assets/cta-tile3-cheque.png" alt="Image of a cheque" class="cta-img-left"/>
                        <p class="cta-txt">If you have taken out PPI (Payment Protection Insurance) on any loans or credit cards in the last 6-10 years, then you may be eligible for a claim.</p>
                        </div>
					</li>
                    <?php */ ?>
                    <li>
						<div id="tile3">
                        	<div class="cta1-title"><img src="<?php bloginfo('url'); ?>/assets/cta-tile1-title.png" alt="WHY CHOOSE CANARY CLAIMS?"/></div>

                            <ul class="cta1 ctalist">
                                <li>Nothing to pay upfront</li>
                                <li>No hidden charges</li>
                                <li>Proven track record</li>
                                <li>High success rate</li>
                            </ul>
                            <ul class="cta2 ctalist">
                                <li>Personal service</li>
                                <li>12.5% fee +VAT (15% Total)</li>
                                <li>Efficient Service</li>
                                <li>View our testimonials</li>
                            </ul>

                        </div>
					</li>
				</ul>
            </div>


            <?php /*?><div class="cta-right">
            	<img src="<?php bloginfo('url'); ?>/assets/you-could-be-owed-thousands.png" />
            	<a class="cta-start" title="START YOUR CLAIM NOW" href="http://www.canaryclaims.co.uk/contact-us/">
            		<span class="alt">Start you claim now</span>
            	</a>
            </div><?php */?>

            </div>
           <?php /*?> <div class="cta-tag"></div><?php */?>
            <div class="cta-shadow"></div>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content(); ?>
            <?php /* ?><div class="read-more-home"></div><?php */?>
        </div>
    </div>

    <?php endwhile; endif; ?>


	</div>

	 <div class="homepage-content-right">
        <?php include('quickclaim-full.php'); ?>
		<div id="right-content-top">
        	<h2 class="testimonial-title">Our testimonials speak for themselves...</h2>
            <a href="<?php echo get_permalink(410); ?>">
            <div class="hp-testimonial">
            <ul id="scroller" class="jcarousel-skin-tango">
            	<li></li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I made a claim with Canary Claims and was very happy with their service. Their staff are very friendly and helpful and were there to guide me throughout the whole process. I would recommend Canary Claims to friends and colleagues.</quote>
                <span class="quote-end">Mr Denis Crisostomo – Compensation: £2,255</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for everything You’ve done!</quote>
                <span class="quote-end">Mrs Myla R Larman – Compensation: £3,982</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Very happy with your professionalism and helpfulness. Very fast and efficient with all the paperwork so that I was hardly involved in the process. I would definitely recommend your services</quote>
                <span class="quote-end">Miss M Molyn – 14/2/13</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would just like to take this opportunity to thank you so much for what you have done for me and for your companies professionalism throughout and I will not hesitate to recommend you to my friends/family.</quote>
                <span class="quote-end">Ms Scott - 28/4/13 – Compensation £1,752</span></div>
                </li>
<?php /*?>                <li>
                <div class="testi-wrap"><quote class="home-scroll">I must say a very big thanks for your hard work on my case. We have finally received our money from the bank. Thanks again may the God of Heaven Bless you all in your Jobs.</quote>
                <span class="quote-end">Yours Faithfully, Burlet & Michael Brown, 25 June 2012<br />(PPI Refund: &pound;5,420)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for all your good work, you are highly recommended. Thank you again.</quote>
                <span class="quote-end">Mr & Mrs Custodio, 15 June 2012<br />(PPI Refund: &pound;6,460)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Please accept my many thanks to Canary Claims for all the help with my PPI. I will be recommending your company and I hope that you pass my thanks on to all who have helped with my claim.</quote>
                <span class="quote-end">Miss Amanda Henderson, 7 May 2012<br />(PPI Refund: &pound;1,614)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would just like to take this opportunity to thank you so much for what you have done for me and for your companies professionalism throughout and I will not hesitate to recommend you to my friends/family.</quote>
                <span class="quote-end">Ms Janice Scott, 28 April 2012 <br />(PPI Refund: &pound;1,752)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for taking on our case and securing such a nice surprise for us.....once again many thanks.</quote>
                <span class="quote-end">Mrs & Mrs Vickers, 12 April 2012<br />(Compensation: &pound;2,446)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Many thanks for assistance in helping me in my claim, any friends who require any assistance in refund for PPI claims I will pass on your information to them once again many thanks.</quote>
                <span class="quote-end">Mr Simon Bryans, 16 February 2012<br />((PPI Refund: &pound;7,734)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Please extend my gratitude to the whole team involved in making my claim successful.</quote>
                <span class="quote-end">Mr Pio JR Fausto, 1 February 2012<br />(PPI refund: &pound;2,494)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">We wish to thank you for all your help in this matter, as we wouldn't have known where to start ourselves. Can you pass this on to everyone involved in this case.</quote>
                <span class="quote-end">Mr & Mrs Stephens, 31 January 2012<br />(PPI refund: &pound;5,318)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for your help processing my claim and I'm so grateful.</quote>
                <span class="quote-end">Miss Prescila Cinese, 16 January 2012<br />(PPI refund: &pound;29,048)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Please accept my sincerest thanks for all you have done, I will of course recommend you to anyone who asks how my claim was dealt with.</quote>
                <span class="quote-end">Mr Steve Adaway, 15 December 2011<br />(PPI refund: &pound;6,546)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">We would like to say a <strong>"BIG THANK YOU"</strong> for assisting us to process all the necessary paperwork for PPI claims. We would not have done it without your big help.</quote>
                <span class="quote-end">Mrs Anarissa Andaya, 8 December 20111<br />(PPI refund: &pound;9,595)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">A big thank you for your help & effort regarding my claims.</quote>
                <span class="quote-end">Miss Maria Janoras, 5 December 2011<br />(PPI refund: &pound;462)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for the work well done.</quote>
                <span class="quote-end">Mr Ernesto Flordeliz, 21 November 2011<br />(PPI refund: &pound;2,634)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for all the support and information you have given me throughout the whole of PPI claims process.</quote>
                <span class="quote-end">Ms Ashleigh Cassidy, 11 November 2011<br />(PPI refund: &pound;2,509)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I'd like to thank you for your help in this matter, and I won't hesitate to recommend you to my friends in the future.</quote>
                <span class="quote-end">Mr Tim Sandom, 9 November 2011<br />(PPI refund: &pound;1,052)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for your services. I have referred a few other people to your company.</quote>
                <span class="quote-end">Ms Evelyn Mwanzia, 2 November 2011<br />(PPI refund: &pound;12,599)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thanks for doing such a great job.</quote>
                <span class="quote-end">Mr Robert Anderson, 25 October 2011<br />(PPI refund: &pound;5,426)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thanks for your great service. Wouldn't have done this without you guys!</quote>
                <span class="quote-end">Miss M Muchelemba, 12 October 2011<br />(PPI refund: &pound;3,172)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to take this opportunity to thank you all for making these things happen. It has been of great help and without you all I wouldn't know I could have claimed a refund from PPI. My sincere thanks to all!</quote>
                <span class="quote-end">Mrs A Ferrer, 17 October 2011<br />(Total PPI refund: &pound;6,426)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I have recommended you to a few more people whom also need help in claiming PPIs back. Thanking you.</quote>
                <span class="quote-end">Mr Keith Dykes, 26 September 2011<br />(PPI Refunded: &pound;5,608)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thanks for all the hard work for both PPI and credit card charges.</quote>
                <span class="quote-end">Mr Ross Tomsett, 12 September 2011<br />(Total Refund: &pound;13,917)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for all your help.</quote>
                <span class="quote-end">Ms Claire Black, 9 September 2011<br />(PPI Refunded: &pound;2,245)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for all your hard work.</quote>
                <span class="quote-end">Mrs Rema Dillon, 7 September 2011<br />(PPI Refunded: &pound;15,281)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to thank you for the assistance you given and all the information you provided. I'm so grateful to have the refund.</quote>
                <span class="quote-end">Miss Lorenza Aquino, 7 September 2011<br />(PPI Refunded: &pound;1,366)</span></div>
                </li>
            	<li>
                <div class="testi-wrap"><quote class="home-scroll">I just want you to know how much I appreciated your help in claiming my PPI from Lombard Direct. Without your intervention it was going to be impossible to get my claim from them.</quote>
                <span class="quote-end">Mr Alfred Mattia, 2 September 2011<br />(PPI Refunded: &pound;551)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for work well done. I got the money as stipulated.</quote>
                <span class="quote-end">Mrs G Nyirenda, 1 September 2011<br />(Refund: &pound;5,300)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for all your hard work to date.</quote>
                <span class="quote-end">Mr P M Summers, 30 August 2011<br />(PPI Refunded: &pound;8,048)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to say thank you and more power to your company... God&nbsp;bless.</quote>
                <span class="quote-end">Mrs E. G. Coronel, July 2011<br />(PPI Refunded: &pound;2,947)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I just wanted to thank you so much for the successful proof of mis-sale of PPI on the Virgin/MBNA card for me.  I was thrilled to be told I was going to receive a cheque, which arrived today!</quote>
                <span class="quote-end">Mrs P Tremain, 29 July 2011<br />(PPI Refunded: &pound;492.51)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I wish to convey my thanks for your help in this matter.</quote>
                <span class="quote-end">Miss C Jones, July 2011<br />(PPI Refunded: &pound;2,650)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">A big thank you for all your help.</quote>
                <span class="quote-end">Mr & Mrs Gardner, 2 July 2011<br />(PPI Refunded: &pound;6,505)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I'm very grateful for the work you have put in with relation to my two card claims and would recommend your company to others.</quote>
                <span class="quote-end">Mrs Pauline Tremain, 28 March 2011</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thanks for your company's efforts - I will recommend you to all.</quote>
                <span class="quote-end">Mr Peter J Gavaghan, 14 February 2011</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to take this opportunity to thank you for the excellent service you have provided regarding this matter.</quote>
                <span class="quote-end">Mr Philip E Hutchinson, 29 December 2010<br />(PPI reclaimed: &pound;2,050)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I certainly didn't feel pestered by yourselves, the whole process has been very simple and easy and it's much appreciated and many thanks.</quote>
                <span class="quote-end">Mr & Mrs D Roberts</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">We would like to thank you for your great help.</quote>
                <span class="quote-end">Mr & Mrs Capuyan, 11 January 2011<br />(Total compensation: &pound;6,400)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I was so pleased with the way your company dealt with my claim that I recommended your services to my husband.</quote>
                <span class="quote-end">Mrs Barbara Gibson, 19 December 2010<br />(Total Compensation: &pound;6,063)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I must thank you for your assistance in winning this claim for me. I have recommended your company to my colleagues.</quote>
                <span class="quote-end">Mr A G Dixey, 25 November 2010<br />(Total Compensation: &pound;9,506.64)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to thank you very much for helping me reclaim my mis-sold PPI.</quote>
                <span class="quote-end">Miss C Lewis, 30 September 2010<br />(Total Compensation: &pound;4,185.04)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to thank you for your help, God bless you! I will recommend you to all my friends.</quote>
                <span class="quote-end">Mrs Felma Patio, 27 September 2010</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to thank you for your help for the insurance claim. It is greatly appreciated.</quote>
                <span class="quote-end">Mr Daniel Flosa, 31 August 2010</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">We would like to also take this opportunity to thank you for your time and assistance in dealing with our matter.</quote>
                <span class="quote-end">Miss K Major, 23 August 2010<br />(Compensation: &pound;9,887.25)</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thanks once again for dealing with my claim, I would not have done it without your help.</quote>
                <span class="quote-end">Miss A Khatun, 7 August 2010</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you so much for your swift and efficient service. I am extremely pleased with the settlement...</quote>
                <span class="quote-end">Mrs J Bellis, 2 June 2010</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to say your company has provided a great service and has been very efficient and have recommended you to my friends.</quote>
                <span class="quote-end">Miss Claire Dawson, 15 May 2010</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to say a quick thank you for the speedy way you have treated my claim, I will certainly be passing your details to my friends and family.</quote>
                <span class="quote-end">Mr Roger Jenks, 27 May 2009</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for your prompt and successful assistance.</quote>
                <span class="quote-end">Mr P Plucknett, May 2009</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you so much for settling our claim with AXA. It is very much appreciated... We would certainly recommend you to others.</quote>
                <span class="quote-end">Mr & Mrs Winfield, 1 April 2009</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">...Thanks for your help and support.</quote>
                <span class="quote-end">Mr Neil Chilcott, 31 March 2009</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I thought that I must take some time to write, to thank you for the service that you are currently providing to resolve my complaints with three different companies... well done to all of your team for a job well done.</quote>
                <span class="quote-end">R P Holden</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">...Thank you very much for your help in achieving a satisfactory outcome.</quote>
                <span class="quote-end">Mr & Mrs A Brown, Oct 2008</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for your assistance in this matter, we are very pleased with the outcome.</quote>
                <span class="quote-end">Mr Welford & Ms Wallace</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Once again I would like to thank you for the speedy and efficient way you dealt with this claim.</quote>
                <span class="quote-end">Mr & Mrs E Anderson</span></div>
                </li>
				<li>
                <div class="testi-wrap"><quote class="home-scroll">...Thank you for a prompt service.</quote>
                <span class="quote-end">Mrs J R Glynn</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">May I thank you from my wife & I for all your help in getting our compensation, we are very grateful and by way of thanks we have forwarded your company name to many of our friends and family. Cheers!</quote>
                <span class="quote-end">Mr D Lane</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you very much for your work and would just like to say how pleased I was with the result, and I have passed your company name & number to family and friends.</quote>
                <span class="quote-end">Mr & Mrs Dowson</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">I would like to take this opportunity to thank you for all the hard work that you have put in to get us the compensation that we have received.</quote>
                <span class="quote-end">Phil & Sue Webb</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">...I received a compensation offer from Lloyds TSB regarding my complaint which you have taken up on my behalf ... I would also like to thank you for a professional service.</quote>
                <span class="quote-end">Dirk Lee</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">Thank you for a very professional service.</quote>
                <span class="quote-end">Mr G Lloyd</span></div>
                </li>
                <li>
                <div class="testi-wrap"><quote class="home-scroll">...I was advised to contact you by a relative who works for an insurance company, as you have an excellent record for successful claims and are an awful lot cheaper than others.</quote>
                <span class="quote-end">Mr S Kimberley</span></div>
                </li><?php */?>
              </ul>
            </div>
            </a>
            <p class="testimonials-link"><a href="<?php echo get_permalink(410); ?>">View all 4 testimonials</a></p>
            <?php if(function_exists('add_footer_snippets')) { add_footer_snippets(); } ?>



			<!--<div id="part2_right_box">

                    <span class="tp"></span>
                    	<h2 class="ticker-title">The company people recommend...</h2>


<div id="recommend">
		<div id="slide">




<!-- You may remove last cell below (<td> ... </td>) to get rid of Speed-change -->

<!--<script language="JavaScript">document.write('<table><tr><td width='+340+'px>');if (document.getElementById || document.all){document.write('<span style="height:'+150+'px;"><div style=";position:relative;overflow:hidden;width:'+350+'px;height:'+330+'px;clip:rect(0 '+swidth+'px '+150+'px 0);background-color:'+sbcolor+';" onMouseover="sspeed=0;" onMouseout="sspeed=restart"><div id="slider" style="position:relative;width:'+swidth+'px;"></div></div></span>')}</script>
<ilayer width=&{swidth}; height=&{sheight}; name="slider1" bgcolor=&{sbcolor};><layer name="slider2" width=&{swidth}; onMouseover="sspeed=0;" onMouseout="sspeed=restart"></layer></ilayer></td></tr></table>
<!-- end of code between BODY TAGS
</div>
		</div>

                    </div>
                    <div id="part2_right_box3"></div>




        -->
        </div>
		<div class="right-content-bottom">
     	<h2>PPI News</h2>

                    <?php
					//require('wp-blog-header.php');
					$args = array(
					'numberposts'     => 2,
					'category'        => 4,
					'orderby'         => 'post_date',
					'order'           => 'DESC',
					'post_type'       => 'post',
					'post_status'     => 'publish' );
					$myposts=get_posts( $args );
					foreach($myposts as $post) {
					setup_postdata($post);
					?>
                    <div class="homepage-news">
                    <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					  //$previewpic = get_the_post_thumbnail($post->ID, 'thumbnail');
					 //echo $previewpic;
						$image_id = get_post_thumbnail_id();
						$image_url = wp_get_attachment_image_src($image_id,'thumbnail');
						$image_url = $image_url[0];
						?><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="nofollow"><img src="<?php echo $image_url; ?>" title="<?php the_title(); ?>" class="hp-news"/></a><?php

					} ?>


					<?php
                    //echo get_the_post_thumbnail($page->ID, 'thumbnail'); ?>
					<div class="newsfeed-title"><a href="<?php the_permalink(); ?>" class="newsfeed"><?php the_title(); ?></a></div>

					 <?php
					$contentstring = $post->post_excerpt;
					$contentstring = preg_replace('/<img[^>]+./','', $contentstring);
					//echo $contentstring;
					// if(function_exists('neat_trim')) $trimtitle = neat_trim($titlestring, 30);
					if(function_exists('neat_trim')) $trimcontent = neat_trim($contentstring, 90);
						//the_excerpt();
					?>
                    <p class="newsfd"><?php echo $trimcontent; ?></p>
                    <div class="clearfix"></div>
					</div>
                    <?php
					} // end of for loop
					?>
                </div>
		</div>
	</div>

            <div class="content-bottom"></div>

        </div>

	<?php
		require('can-footer.php');
	?>

</body>

</html>