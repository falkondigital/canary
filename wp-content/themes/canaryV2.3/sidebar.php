<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Canary Claims 
 * @since Canary Claims 2.1
 */
?>

		<div id="sidebar" class="widget-area" role="complementary">
			<ul>

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
    		<li class="quick-claim">
    		<?php // include('quickclaim.php'); ?>
            <?php include('quickclaim-news.php'); ?>
            </li>
	
			<?php 
			$args = array(
					'numberposts'     => 5,
					'orderby'         => 'post_date',
					'order'           => 'DESC',
					'post_type'       => 'post',
					'post_status'     => 'publish' );
			$posts_array = get_posts($args); ?>
			<li class="recent-list">
            	<p class="sidebar-recent"><img src="<?php bloginfo('url'); ?>/assets/blog-recentposts.gif" ALT="RECENT POSTS"/></p>
				<div class="sidebar-wrap">
				<ul>
					<?php foreach( $posts_array as $post ){ ?>
					<li><a href="<?php the_permalink() ?>" class="sidebar"><?php the_title(); ?></a></li>
					<?php } ?>
				</ul>
                </div>
			</li>
			<li class="cats-list">
            <p class="sidebar-category"><img src="<?php bloginfo('url'); ?>/assets/blog-categories.gif" ALT="Categories"/></p>
				<div class="sidebar-wrap">
				<ul>
				<?php wp_list_categories('show_count=1&title_li='); ?>
				</ul>
                </div>
			</li>
			<li class="arc-list" id="archives">
            <p class="sidebar-archives"><img src="<?php bloginfo('url'); ?>/assets/blog-archives.gif" ALT="Monthly archives"/></p>
            <div class="sidebar-wrap">
				<ul>
				<?php get_archives('monthly', '', 'html', '', '', TRUE); ?>
                <?php //wp_get_archives( 'type=monthly' ); ?>
				</ul>
                </div>
			</li>
		<?php endif; // end primary widget area ?>
			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul>
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
