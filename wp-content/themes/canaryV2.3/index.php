<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Canary Claims
 * @since Canary Claims 2.1
 */
include('quickclaim.php');
get_header(); ?>
<body id="news">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
	<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="blog-summary-wrapper">
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        
         
 
        <div class="entry-sum">
        <p class="blogheading"><a href="<?php the_permalink() ?>" class="blogheading" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
        <?php the_excerpt(); ?>
        </div>
		
        <p class="postmetadata-right"><span>Posted in <?php the_category(', ') ?> |</span><span> By <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="nofollow">
									<?php printf( __('%s'),get_the_author());?></a> |</span><span> <?php the_time('F jS, Y') ?> | <?php edit_post_link('Edit', '', ' | '); ?></span><span>  <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span></p>
                                    
       
       
        <br />
    </div>
    </div>
    <div class="divide-horizontal"></div>
	<?php endwhile; ?>
		
    <div class="blognavigationbottom">
        	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php //get_search_form(); ?>

	<?php endif; ?>
     </div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('can-footer.php'); 
	?>
    
			
</body>

</html>