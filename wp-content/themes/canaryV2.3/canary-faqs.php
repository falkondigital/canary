<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: FAQs page
 */

get_header(); ?>

<body id="faq">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content(); ?>

        </div>
    </div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
	
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
</body>

</html>