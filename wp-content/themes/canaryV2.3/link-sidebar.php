<div id="payment-sb">
	<span class="sb-top"></span>
    <a href="<?php echo get_permalink(5); ?>"><span class="sb-link pp"><?php _e('Payment Protection'); ?></span></a>
    <a href="<?php echo get_permalink(315); ?>"><span class="sb-link miss-sold-ppi"><?php _e('Mis sold PPI'); ?></span></a>
    <a href="<?php echo get_permalink(486); ?>"><span class="sb-link cc-ppi"><?php _e('Credit Card PPI'); ?></span></a>
    <a href="<?php echo get_permalink(489); ?>"><span class="sb-link mort-ppi"><?php _e('Mortgage PPI'); ?></span></a>
    <a href="<?php echo get_permalink(470); ?>"><span class="sb-link claim-ppi"><?php _e('How to Claim PPI'); ?></span></a>
    <span class="sb-bottom"></span>
</div>