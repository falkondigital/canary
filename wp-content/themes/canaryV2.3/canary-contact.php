<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Contact page
 */
/*$flag = false;
if($_POST['sbtbtn'])
{
	$details = '';
	if($_POST['numofBC'] != 'notset')
		$details .= 'Number of Loans : '.$_POST['numofBC']."\n";

	if($_POST['numofCC'] != 'notset')
		$details .= 'Number of Credit Cards : '.$_POST['numofCC']."\n";

	if($_POST['numofPPI']  != 'notset')
		$details .= 'Number of Policies : '.$_POST['numofPPI']."\n";

	if($_POST['name'])
		$name = $_POST['name'];

	if($_POST['email'])
		$email = $_POST['email'];

	if($_POST['address'])
		$address = $_POST['address'];

	if($_POST['tel'])
		$tel = $_POST['tel'];

		$message = "Hi,\n\nYou have recieved a mail from the web form\n\nName : $name\nEmail : $email\nPhone : $tel\nAddress : $address\n";

		$to      = 'steve@seo-creative.co.uk';
		$subject = 'Mail from web form';
		$headers = 'From: noreply@canaryclaims.co.uk' . "\r\n" .
		'Reply-To: noreply@canaryclaims.co.uk' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		if(mail($to, $subject, $message.$details."\n\nThank you\nWebmaster", $headers))
		$flag = true;

}*/
$from = $_GET["from"];
include('quickclaim.php');
get_header(); 
?>

<body id="contact">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        	<div>
            	<h1>Contact Us</h1>
                
                
                <div class="col1-contact">
					
		  			<?php include('contact-form2.php'); ?>

                </div>
                
                
                <div class="col2-contact">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="post" id="post-<?php the_ID(); ?>">
                        <div class="entry">
                            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                                </div>
                            </div>
                            <div class="contact-downloads"><p><strong><span style="color:#4b8a35;">Download Application Forms:</span></strong></p>
                            <?php /* ?><p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/PPI_claim.pdf" title="PPI_claim.pdf">Application form 1 - Payment Protection Insurance</a></p><?php */ ?>
                            <p class="pdfdown"><em>Application form 1 - Payment Protection Insurance</em><br>(currently unavailable)</p>
                            <?php /*?><p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/Credit_Card_Charge_Claim.pdf" title="Credit_Card_Charge_Claim.pdf">Application form 2 - Credit Card Charges</a></p><?php */?>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                    <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
                </div>
    
			</div>
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
</body>
<script language="javascript">
function toggleNumSelect(elemDom){
if(elemDom.checked) {elemDom.parentNode.getElementsByTagName("span")[0].style.display = "inline";}
else {elemDom.parentNode.getElementsByTagName("span")[0].style.display = "none";}
}
</script>
</html>