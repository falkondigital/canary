
<?php
/** Template Name: PPI Landing Page V2
**/
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		if ($_POST['formtype'] == 'claim')
		{
			$msg = '';
			$title = '';
				
			switch($_POST['claimtitle']){
				case 'Title': $title = ''; break;
				case 'Mr': $title = $title; break;
				case 'Mrs': $title = $title; break;
				case 'Miss': $title = $title; break;
			}
			
			switch($_POST['claimclaims']){
				case 'PPI Claims': $ppitotal = '0'; break;
				default: $ppitotal = $_POST['claimclaims']; break;
			}
			
			if(($_POST['claimfirstname'] != 'Name') &&
			($_POST['claimcontactnum'] != 'Telephone number') &&
			($_POST['claimaddress'] != 'Address') &&
			($_POST['claimtown'] != 'Town') &&
			($_POST['claimpostcode'] != 'Post code') &&
			($_POST['claimemail'] != 'Email'))
			{
				if(filter_var($_POST['claimemail'], FILTER_VALIDATE_EMAIL)) {
				
					$to = "kamran@canaryclaims.co.uk";
					//$to = "jonathan@seo-creative.co.uk";
					$subject = "Callback Submission";
					$message = "Form Sent from Canary Claims. Please callback regarding PPI, my details are:\r\n";
					$message .="Contact Name: ".$title. " " .$_POST["claimfirstname"]. " " .$_POST["claimlastname"]. "\r\n";
					$message .="Contact Num: ".$_POST["claimcontactnum"]. "\r\n";
					$message .="Address: ".$_POST["claimaddress"]. "\r\n";
					$message .="Town: ".$_POST["claimtown"]. "\r\n";
					$message .="Postcode: ".$_POST["claimpostcode"]. "\r\n";
					$message .="Email Address: ".$_POST["claimemail"]. "\r\n";
					$message .="No. PPI Claims: ".$ppitotal. "\r\n";
					$headers = "From: Canaray Claims <noreply@canaryclaims.co.uk> \r\n";
					$headers .="Reply-To: Canaray Claims <kamran@canaryclaims.co.uk> \r\n" .
					$headers .='X-Mailer: PHP/' . phpversion();
					mail($to, $subject, $message, $headers) or die ("Failure");
				
				
				
					$to = $_POST['claimemail'];
					$subject = "Callback Submission";
					$message = "Thank you for your enquiry. An application pack has been sent to you by first class post. Please complete and return in order to start your claim";
					$headers = "From: Canaray Claims <noreply@canaryclaims.co.uk> \r\n";
					$headers .="Reply-To: Canaray Claims <info@canaryclaims.co.uk> \r\n" .
					$headers .='X-Mailer: PHP/' . phpversion();
					mail($to, $subject, $message, $headers) or die ("Failure");
					
					$msg = '<div id="messagebar">Thank you for submitting your claim. Someone will respond shortly.</div>';
										
					$_POST['claimfirstname'] = 'Name';
					$_POST['claimcontactnum'] = 'Telephone number';
					$_POST['claimaddress'] = 'Address';
					$_POST['claimtown'] = 'Town';
					$_POST['claimpostcode'] = 'Post code';
					$_POST['claimemail'] = 'Email';
					$_POST['claimclaims'] = 'Number of PPI claims';
					
					header( 'Location:'. get_permalink(61) ) ;

				}
				else {
					$msg = '<div id="errorbar">Please enter a correct email address.</div>';	
				}	
			}
			else
			{
				$msg =  '<div id="errorbar">Please enter all details correctly.</div>';
			}
		}
	}



get_header(); ?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="<?php echo bloginfo('template_directory'); ?>/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">

jQuery().ready(function() {
	$.validator.addMethod("noPlaceholder", function(value, element) 
    {
     switch (element.value) 
     {
      	case "Name": 
          		return false;
		case "Email": 
          		return false;
		case "Address": 
       			return false;
		case "Town": 
      			return false;
		case "Post code": 
       			return false;
		case "Telephone number": 
       			return false;
		default:
				return true;
     }
	})
	
	function IsEmail(email) {
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	
	// validate signup form on keyup and submit
	$("#claimform").validate({
		rules: {
			claimtitle: {
				required: true				
			},
			claimfirstname: {
				required: true
			},
			claimlastname: {
				required: true
			},
			claimcontactnum: {
				required: true,
				digits: true
			},
			claimaddress: {
				required: true
			},
			claimtown: {
				required: true
			},
			claimpostcode: {
				required: true
			},
			claimemail: {
				required: true,
				email: true
			}
		},
		messages: {
			claimtitle: "",
			claimfirstname: "",
			claimlastname: "",
			claimcontactnum: "",
			claimaddress: "",
			claimtown: "",
			claimpostcode: "",
			claimemail: "",
			claimclaims: "",
			noPlaceholder: ""
		}
	});
});
</script>



<body>
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">

            <div id="claim">
            	<p class="claim-title">Start your claim now!<p>
               	<p class="claim-sub">You could be owed thousands...<p>
                <p>Fill in the form below to start your claim, and the Direct PPI team will handle the rest!<p>
                
 
                <form action="" method="post" id="claimform" >
                <select name="claimclaims" style="float:left;margin-bottom: 18px;">
                <option value="1 PPI Claim" selected="selected">1 PPI Claim</option>
                <?php $total=9;$i=1;
					while($i<$total){
						$i++;
						echo '<option value="'.$i.'">'.$i.'</option>';
					}
				?>
                </select>
                
                <select name="claimtitle" style="float:left;clear:both;">
                	<option value="Mr" selected="selected">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Miss">Miss</option>
                </select> 
                
                
                <input name="claimfirstname" id="claimfirstname" type="text" class="box noPlaceholder" style="float:left;margin-left:20px;" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimfirstname']) && ($_POST['claimfirstname'] != "Name")){echo $_POST['claimfirstname'];}else{echo "Name";}?>"/><br />
                <input name="claimemail" id="claimemail" type="text" class="box  noPlaceholder maxw" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimemail']) && ($_POST['claimemail'] != "Email")){echo $_POST['claimemail'];}else{echo "Email";}?>"/>
                <input name="claimaddress" id="claimaddress" type="text" class="box  noPlaceholder maxw" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimaddress']) && ($_POST['claimaddress'] != "Address")){echo $_POST['claimaddress'];}else{echo "Address";}?>"/>
                <input name="claimtown" id="claimtown" type="text" class="box  noPlaceholder maxw" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimtown']) && ($_POST['claimtown'] != "Town")){echo $_POST['claimtown'];}else{echo "Town";}?>"/>
                <input name="claimpostcode" id="claimpostcode" type="text" class="box  noPlaceholder" style="width:136px;float:left;" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimpostcode']) && ($_POST['claimpostcode'] != "Post code")){echo $_POST['claimpostcode'];}else{echo "Post code";}?>"/>
                <input name="claimcontactnum" id="claimcontactnum" type="text" class="box  noPlaceholder" style="float:left;margin-left:20px;" onFocus="if (this.value==this.defaultValue) this.value = ''" onBlur="if (this.value=='') this.value = this.defaultValue" value="<?php if(isset($_POST['claimcontactnum']) && ($_POST['claimcontactnum'] != "Telephone number")){echo $_POST['claimcontactnum'];}else{echo "Telephone number";}?>"/>
                       
				<input name="formtype" value="claim" type="hidden"/>
                <input type="submit" class="submit-btn" value=" " align="middle"/>
                </form>
                <div class="clearfix"></div>
                <?php if($msg != '') echo $msg; ?>
 
            </div>
            
            <div class="usp-img"><img src="<?php echo bloginfo('template_directory'); ?>/images/USP.jpg" width="370" height="520" alt="Why Canary Claims?" /></div>
            <div class="usp-img320"><img src="<?php echo bloginfo('template_directory'); ?>/images/USP320.jpg" width="320" height="520" alt="Why Canary Claims?" /></div>
            
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="post" id="post-<?php the_ID(); ?>" style="clear:both;padding-top:20px;">
                    <div class="entry">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; endif; ?>
			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
            
		</div>
		<div class="content-bottom"></div>
	</div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
</body>

</html>
