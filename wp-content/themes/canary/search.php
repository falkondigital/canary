<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 */

get_header();
?>

<body id="news">
	<?php 
	  require('cr-navigation.php');
	?>
    
    <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">

				<?php if (have_posts()) : ?>
				
				<h2 class="center">Search Results</h2>
				
				<p>You have searched the <a href="<?php echo bloginfo('url'); ?>/"><?php echo bloginfo('name'); ?></a> archives for <strong>'<?php the_search_query(); ?>'</strong>. If you are unable to find anything in these search results, you can try one of the links in the navigation on the left or try another search term.</p>
				
				<?php while (have_posts()) : the_post(); ?>

					<div <?php post_class() ?>>
						<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" class="blogheading" rel="bookmark" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						<div class="small">Posted on <?php the_time('l, F jS, Y') ?></div>

						<p class="postmetadata">Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
						<br />
					</div>

				<?php endwhile; ?>

				<div class="blognavigation">
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
				</div>

				<?php else : ?>

					<h2 class="center">No posts found. Try a different search?</h2>
                    <p>There are no matches for your search term. Try searching again or browse the website using the navigation.</p>
					<?php get_search_form(); ?>

				<?php endif; ?>
</div>
	
	 </div>
     <div class="content-bottom"></div>    			
	<?php 
		require('cr-footer.php'); 
	?>
    
			
</body>

</html>