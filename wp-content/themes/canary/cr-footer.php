<?php
/*
*
*
*/

?>

<div class="footer-top"></div>
    <div class="footer">
    	<div class="footer-links">
    <a href="<?php bloginfo('url'); ?>" class="footer-link">Home</a> | <a href="<?php bloginfo('url'); ?>/news/" class="footer-link">News</a> | <a href="<?php bloginfo('url'); ?>/faqs/" class="footer-link">Frequently Asked Questions</a> | <a href="<?php bloginfo('url'); ?>/privacy/" class="footer-link">Privacy</a> | <a href="<?php bloginfo('url'); ?>/terms-and-conditions/" class="footer-link">Terms and Conditions</a> | <a href="<?php bloginfo('url'); ?>/contact-us/" class="footer-link">Contact</a> | <a href="<?php bloginfo('url'); ?>/payment-protection/" class="footer-link">Reclaim Mis Sold PPI</a> | <a href="<?php bloginfo('url'); ?>/no-win-no-fee/" class="footer-link">No Win No Fee</a> | <a href="<?php bloginfo('url'); ?>/refer-a-friend/" class="footer-link">Refer a Friend</a> | <a href="<?php bloginfo('url'); ?>/category/testimonials/" class="footer-link">Testimonials</a>
    	</div>
        <div class="footer-left">
        	<p class="footer-text">Claims Thru Us Limited trading as Canary Claims is regulated by the Ministry of Justice in respect of regulated claims management activities; its registration is recorded on the website: <a href="http://www.claimsregulation.gov.uk" rel="nofollow" class="footer-link">www.claimsregulation.gov.uk</a> <strong>Authorisation Number: CRM2233</strong> &copy; Canary Claims. All Rights Reserved 2010</p>
        </div>
        <div class="footer-right">
        	<img src="<?php bloginfo('url'); ?>/assets/ico-logo.jpg" alt="ico logo" />
        </div>
    </div>
    <div class="footer-bottom"></div>
    
</div>
