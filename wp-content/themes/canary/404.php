<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 */

get_header();?>

<body>
	<?php 
	  require('cr-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
			<h2 class="center">We can't find the page you're looking for!</h2>
			<p>Hmm, you seem to be lost! You can use the navigation above or use the search form to try and find the page you were looking for.</p>

			</div>
		</div>
	 </div>
        		
      <div class="content-bottom"></div>              
                	
	<?php 
		require('cr-footer.php'); 
	?>
     			
    
			
</body>

</html>