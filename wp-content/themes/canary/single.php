<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 */

get_header();
?>

<body id="news">
	<?php 
	  require('cr-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<h1><?php the_title(); ?></h1>
            <div class="small">Posted on <?php the_time('l, F jS, Y') ?></div>

			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		
		<div class="blognavigationbottom">
			<div class="alignleftbrac"><?php previous_post_link('&laquo;') ?></div><div class="alignleft"><?php previous_post_link('%link') ?></div>
			<div class="alignrightbrac"><?php next_post_link('&raquo;') ?></div><div class="alignright"><?php next_post_link('%link') ?></div>
		</div>

		<?php comments_template(); ?>
    
        <?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

		<?php endif; ?>
</div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('cr-footer.php'); 
	?>			
</body>

</html>