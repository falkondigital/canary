<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Homepage
 */

get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('url'); ?>/js/ticker.js"></script>

<body id="home" onLoad="start();">
	<?php 
	  require('cr-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        	<div class="calltoaction1">
            <?php if ( function_exists( 'get_smooth_slider' ) ) {
     get_smooth_slider(); }
	 ?>
            	<!--<div class="cta-left">
                    <p class="cta-title1">Why Canary Claims for your PPI reclaims?</p>
                    <p>Here are just a few of the reasons you should use Canary Claims for your PPI reclaims, consectetur adipiscing elit. Morbi est est, lobortis non pretium interdum ultrices: </p>
                </div>
                <div class="cta-right">
                	 <p class="cta-title2">You could be owed</p>
                     <p class="cta-title3">thousands!</p>
                     <p class="cta-title4">If you were mis sold payment<br/>protection insurance, you can<br/>claim it back.</p>
                </div>-->
            </div>
            <div class="cta-right">
            <div class="cta-right-tit">You could be owed <font style="font-size:47px">thousands!</font></div>
            <div class="cta-right-text">If you were mis sold payment protection insurance, you can claim it back.</div>
            <a class="cta-more" href="<?php bloginfo('url'); ?>/contact-us/?from=ppi" title="START YOUR CLAIM NOW"><span class="alt">Start you claim now</span></a>
            </div>
            
            <div class="homepage-content-left">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

        </div>
    </div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
	
	 <div class="homepage-content-right">
		<div id="right-content-top">
        
        
        
        
			<div id="part2_right_box">
                    <span class="tp"></span>
                    	<p class="ticker-title">The company people recommend...</p>
                        

<div id="recommend">
		<div id="slide">




<!-- You may remove last cell below (<td> ... </td>) to get rid of Speed-change -->

<script language="JavaScript">document.write('<table><tr><td width='+340+'px>');if (document.getElementById || document.all){document.write('<span style="height:'+150+'px;"><div style=";position:relative;overflow:hidden;width:'+350+'px;height:'+330+'px;clip:rect(0 '+swidth+'px '+150+'px 0);background-color:'+sbcolor+';" onMouseover="sspeed=0;" onMouseout="sspeed=restart"><div id="slider" style="position:relative;width:'+swidth+'px;"></div></div></span>')}</script>
<ilayer width=&{swidth}; height=&{sheight}; name="slider1" bgcolor=&{sbcolor};><layer name="slider2" width=&{swidth}; onMouseover="sspeed=0;" onMouseout="sspeed=restart"></layer></ilayer></td></tr></table>
<!-- end of code between BODY TAGS -->
</div>
		</div>

                    </div>
                    <div id="part2_right_box3"></div>
		
        
        
        
        
        </div>
		<div class="right-content-bottom">
     	<h2>Latest PPI claim news</h2>
     				
                    <?php
					require('wp-blog-header.php');
					$posts=get_posts('showposts=2');
					foreach($posts as $post) {
					setup_postdata($post);
					?>
					<a href="<?php the_permalink(); ?>" class="newsfeed"><?php the_title(); ?></a>
					<div class="smallfeed"><?php the_time('l, F jS, Y') ?></div>
					 
					 <?php
					$metadesc = get_post_meta($post->ID, 'aioseop_description', true);
					if ($metadesc) {
					   echo $metadesc;
					} else {
					   the_excerpt();
	
					}
					?><div class="readmorespace">
                    	<a href="<?php the_permalink() ?>" rel="nofollow">Read more...</a>
                    </div><?php
					} // end of for loop
					?>
                    
                </div>
		</div>
	</div>
            
            <div class="content-bottom"></div>
            
        </div>
        			
	<?php 
		require('cr-footer.php'); 
	?>
    
</body>

</html>