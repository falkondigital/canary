<?php
/*
*
*
*/

?>

<div class="main">
	<div class="header-top"></div>
    <div class="header-middle">
    	<div class="header-middle-left">
        	<img src="<?php bloginfo('url'); ?>/assets/canary-claims-logo.jpg" alt="Canary Claims logo" class="main-logo" />
        </div>
        <div class="header-middle-right">
        	<img src="<?php bloginfo('url'); ?>/assets/tel-0800-634-8668.jpg" alt="0800 634 8668" class="telephone-number" />
             <div class="searchdiv">
            	<?php get_search_form(); ?>
            </div>
        </div>
    </div>
    <div class="header-bottom"></div>
    <div class="navigation">
    	<ul id="nav">
            <li><a class="home-nav" href="<?php bloginfo('url'); ?>"><span class="alt">Home</span></a></li>
            <li><a class="about-nav" href="<?php bloginfo('url'); ?>/about-us/"><span class="alt">About</span></a></li>
            <li><a class="credit-nav" href="<?php bloginfo('url'); ?>/credit-card-charges/"><span class="alt">Credit Card Charges</span></a></li>
            <li><a class="payment-nav" href="<?php bloginfo('url'); ?>/payment-protection/"><span class="alt">Payment Protection</span></a></li>
            <li><a class="win-nav" href="<?php bloginfo('url'); ?>/no-win-no-fee/"><span class="alt">No Win No Fee</span></a></li>
            <li><a class="refer-nav" href="<?php bloginfo('url'); ?>/refer-a-friend/"><span class="alt">Refer a Friend</span></a></li>
            <li><a class="faq-nav" href="<?php bloginfo('url'); ?>/faqs/"><span class="alt">FAQ</span></a></li>
            <li><a class="news-nav" href="<?php bloginfo('url'); ?>/news/"><span class="alt">News</span></a></li>
            <li><a class="contact-nav" href="<?php bloginfo('url'); ?>/contact-us/"><span class="alt">Contact</span></a></li>
        </ul>
    </div>