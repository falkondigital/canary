<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Credit Card Charges page
 */

get_header(); ?>

<body id="credit">
	<?php 
	  require('cr-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
        <h1>Credit Card Charges</h1>
        <div>
		<div class="col1">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

        </div>
    </div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
    
    </div>
    
    <div class="col2">
    
    	<div id="body-yellow"><img src="<?php bloginfo('url'); ?>/assets/what-shoud.gif" alt="01" width="193" height="75" class="lft"/><a href="<?php bloginfo('url'); ?>/contact-us/?from=ccc"><img src="<?php bloginfo('url'); ?>/assets/option-01.jpg" alt="02" width="288" height="82" border="0" /></a><a href="<?php bloginfo('url'); ?>/pdf/Credit_Card_Charge_Claim.pdf"><img src="<?php bloginfo('url'); ?>/assets/option-02.gif" alt="03" width="287" height="89" border="0" /></a></div>

    
    </div>
    </div>
	
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('cr-footer.php'); 
	?>
    
</body>

</html>