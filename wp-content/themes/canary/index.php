<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 */

get_header(); ?>
	
<body id="news">
	<?php 
	  require('cr-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">
	<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        <h2><a href="<?php the_permalink() ?>" class="blogheading" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
        <div class="small">Posted on <?php the_time('l, F jS, Y') ?></div>
 
        <div class="entry">
            <?php the_content('Read the rest of this entry &raquo;'); ?>
        </div>

        <p class="postmetadata">Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
        <br />
    </div>
	<?php endwhile; ?>
		
    <div class="blognavigation">
        <div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
        <div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
    </div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>
     </div>
	
	 </div>
    
    <div class="content-bottom"></div> 
        			
	<?php 
		require('cr-footer.php'); 
	?>
    
			
</body>

</html>