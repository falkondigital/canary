<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Form Sent page
 */

get_header(); ?>
<SCRIPT language="JavaScript" type="text/javascript">
<!-- Yahoo!
window.ysm_customData = new Object();
window.ysm_customData.conversion = "transId=,currency=,amount=";
var ysm_accountid  = "109LGJ0B2K43737CCURHPR026U8";
document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
+ "SRC=//" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
+ "></SCR" + "IPT>");
// -->
</SCRIPT>
<body id="contact">
	<?php 
	  require('can-navigation.php');
	?>
   
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        	<div>
            	<h1>Thank you</h1>
                
                
                <div class="col1">
                 <div id="contact-bg-outer">
		  <div id="contact-bg-inner">
					
		  <?php
	if ($_POST) {
		$debugStrCr = "\n";
		//$to = "enquiry@canaryclaims.co.uk";
		$to = "info@seo-creative.co.uk, jonathan@seo-creative.co.uk";

		$subject = "An Enquiry From CanaryClaims.co.uk";
		$from = "noreply@canaryclaims.co.uk";
		$headers = 'From: noreply@canaryclaims.co.uk';
		$basic = "User selected Method from Unchecked Checkboxes .. " . $debugStrCr . $debugStrCr;

		$extra ="";
		$mainApplyItem ="";
		if($_POST["alsoppi"]) $extra .= " - Important - Also Contact Me for Payment Protection" . $debugStrCr;
		//if($_POST["alsobc"]) $extra .= " - Important - Also Contact Me for Loans" . $debugStrCr;
		//if($_POST["alsoccc"]) $extra .= " - Important - Also Contact Me for Credit Card Charges" . $debugStrCr;
		//if($_POST["appccc"]) $basic .= "Contact Me for Credit Card Charges" . $debugStrCr;
		//if($_POST["appbc"]) $basic .= "Contact Me for Loans" .  $debugStrCr;
		if($_POST["appppi"]) $basic .= "Contact Me for Payment Protection" . $debugStrCr;
		//if($_POST["numofCC"])$numofCC = $_POST["numofCC"];
		//if($_POST["numofBC"])$numofBC = $_POST["numofBC"];
		if($_POST["numofPPI"])$numofPPI = $_POST["numofPPI"];
		if($_POST["applyNoSelected"]) {
			ini_set('sendmail_from',"noreply@canaryclaims.co.uk");
			$body  = "An enquiry from a Canary Claim User: " . $debugStrCr . $debugStrCr;
			$body .= "Title: ".trim($_POST['title']). $debugStrCr;
			$body .= "Name: ".trim($_POST['username']). $debugStrCr;
			$body .= "Email: ".trim($_POST['email']). $debugStrCr;
			$body .= "Telephone: ".trim($_POST['tel']). $debugStrCr . $debugStrCr;
			$body .= "Address: ". $debugStrCr . trim($_POST['adtest']). $debugStrCr . $debugStrCr;
			//if($_POST['policy_number']) $body .= "Number of policies: ".trim($_POST['policy_number']);
			$body .= "User got To Contact Page without Previously Selecting Method Options " . $debugStrCr;
			if($basic == "") {
			  $basic = "Send All Forms - No Method Detected or User Forgot to Check Checkbox " . $debugStrCr;
			} else {
			  ///
			}
		 $body .=  $basic . $debugStrCr;
		 //if($numofCC && $numofCC != "notset") $body .=  "Number of Credit Cards: " . $numofCC;
		 //if($numofBC && $numofBC != "notset") $body .=  " Number of Loans: " .$numofBC;

		 if($numofPPI && $numofPPI != "notset") $body .= " Number of PPI Policies: " . $numofPPI;
			//####echo $to. $subject . $body;//####
			mail($to,$subject,$body,$headers);
			//header("Location: thanks.html");
	  } else {
		  if($_POST["mainApplyItem"]) $mainApplyItem .= "I Want to be Contacted For: " . $_POST["mainApplyItem"];
		  ini_set('sendmail_from',"noreply@canaryclaims.co.uk");
		  $body  = "An enquiry from a Canary Claim User: " . $debugStrCr . $debugStrCr;
		  $body .= "Title: ".trim($_POST['title']). $debugStrCr;
		  $body .= "Name: ".trim($_POST['username']). $debugStrCr;
		  $body .= "Email: ".trim($_POST['email']). $debugStrCr;
		  $body .= "Telephone: ".trim($_POST['tel']). $debugStrCr . $debugStrCr;
		  $body .= "Address: " . $debugStrCr .trim($_POST['adtest']). $debugStrCr . $debugStrCr;
		  //if($_POST['policy_number']) $body .= "Number of policies: ".trim($_POST['policy_number']);
		  if($basic == "") $basic = "Send All Forms - No Method Detected or User Forgot to Check";
		  $body .=  $mainApplyItem . $debugStrCr . $debugStrCr;
		  $body .=  $extra . $debugStrCr;
		  //if($numofCC && $numofCC != "notset") $body .=  "Number of Credit Cards: " . $numofCC;
		  //if($numofBC && $numofBC != "notset") $body .=  " Number of Loans: " .$numofBC;

if($numofPPI && $numofPPI != "notset") $body .= " Number of PPI Policies: " . $numofPPI;
		  //####echo $to . $subject . $body;//####
		  mail($to,$subject,$body,$headers);
		  //header("Location: thanks.html");

		}
		$bodyCONFIRM = "Thank you for your application. You will soon receive a letter together with the application forms in order to initiate your claim.

If you have any questions or suggestions regarding your enquiry please call us on 0800 634 8668 or email: claims@canaryclaims.co.uk.

Regards,

Claims Team
Canary Claims";
		$subjCONFIRM = "Your CanaryClaims Application";
		$toCONFIRM   = trim($_POST['email']);
		$fromCONFIRM = "noreply@canaryclaims.co.uk";
		$headersCONFIRM = 'From: noreply@canaryclaims.co.uk';
		//ini_set('sendmail_from',"claims@canaryclaims.co.uk");
		mail($toCONFIRM,$subjCONFIRM,$bodyCONFIRM,$headersCONFIRM);
	}
?>
          
       
          <p style="margin-left: -5px; padding-right:14px;">Thank you for contacting us. You should receive a confirmation email and we will contact you soon.</p>
  
                    </div></div>
                    
                   
                </div>
                
                
                <div class="col2">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
				</div>
        	</div>
            <div class="contact-downloads"><p><strong><span style="color:#4b8a35;">Download Application Forms:</span></strong></p>
            <p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/PPI_claim.pdf" title="PPI_claim.pdf">Application form 1 - Payment Protection Insurance</a></p>
            <p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/Credit_Card_Charge_Claim.pdf" title="Credit_Card_Charge_Claim.pdf">Application form 2 - Credit Card Charges</a></p>
            </div>
    	</div>
    <?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
			</div>
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
<!-- Google Code for Form Enquiry Conversion Page -->
<script type="text/javascript">
<!--
var google_conversion_id = 1040779051;
var google_conversion_language = "en_GB";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "ipvBCLP5RhCrjqTwAw";
var google_conversion_value = 0;
//-->
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1040779051/?label=ipvBCLP5RhCrjqTwAw&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<SCRIPT>
microsoft_adcenterconversion_domainid = 46945;
 microsoft_adcenterconversion_cp = 5050;
 </script>
<SCRIPT SRC="http://0.r.msn.com/scripts/microsoft_adcenterconversion.js"></SCRIPT>
</body>

</html>