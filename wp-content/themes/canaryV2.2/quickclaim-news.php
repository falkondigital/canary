<div class="quick-claim-form-news">
        	<div class="qc-title-news"></div>
            <div class="qc-body-news">
            	
                <?php
				echo '<p class="fill-details">Fill out your details below and start your claim!</p>';				
				if($errormessage){
					echo '<p class="error">'.$errormessage.'</p>';
				}
				?>
                <form name="quickclaim" id="quickclaim" action="" method="post" enctype="application/x-www-form-urlencoded">
					<span class="checkbox">
                        <?php /*?><span class="check-wrap news-check qcppi"><input name="qcppi" id="qcppi" type="checkbox" <?php if($postPPI == 'Yes'){ echo 'checked="checked"'; } ?> /><label for="qcppi">Number of PPI claims</label></span><br /><?php */?>
                        <?php /*?><span class="check-wrap news-check qcppi"><span class="ui-checkbox"><input name="qcppi" id="qcppi" maxlength="2" value="<?php if($postPPI){ echo $postPPI; } else { echo '1'; }?>" onBlur="if (this.value == '') {this.value = '1';}" onFocus="if (this.value == '1') {this.value = '';}" /></span><label for="qcppi">Number of PPI claims</label></span><br /><?php */?>
                    	<span class="check-wrap news-check qcppi">
                                <select name="qcppi" id="qcppi">
                                	<?php
									$i = 1;
									$a = 9;
									
									while($i <= $a){
										if($i == 1){
											echo '<option value="'.$i.'">'.$i.' PPI Claim</option>';
										}
										else{
											echo '<option value="'.$i.'">'.$i.'</option>';
										}
										$i++;
									}
									/*
									if($i > $a){
										echo '<option value="'.$a.'">'.$a.'+</option>';
										$i++;	
									}
									*/
									?>
                                </select>
                        </span><br />
                        <span class="check-wrap news-check qctitle">
                                <select name="qctitle" id="qctitle">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                </select>
                        </span><br />
                    </span>
                    <input name="qcname" id="qcname" value="<?php if($postName){ echo $postName; } else { echo 'Name'; }?>" onBlur="if (this.value == '') {this.value = 'Name';}" onFocus="if (this.value == 'Name') {this.value = '';}"/>
                    <input name="qchousenum" id="qchousenum" value="<?php if($postHouseNum){ echo $postHouseNum; } else { echo 'House number'; }?>" onBlur="if (this.value == '') {this.value = 'House number';}" onFocus="if (this.value == 'House number') {this.value = '';}" />
                    <input name="qcpostcode" id="qcpostcode" value="<?php if($postPostcode){ echo $postPostcode; } else { echo 'Post code'; }?>" onBlur="if (this.value == '') {this.value = 'Post code';}" onFocus="if (this.value == 'Post code') {this.value = '';}" />
                    <input name="qcemail" id="qcemail" value="<?php if($postEmail){ echo $postEmail; } else { echo 'Email address'; }?>" onBlur="if (this.value == '') {this.value = 'Email address';}" onFocus="if (this.value == 'Email address') {this.value = '';}" />
                    <input name="qcphone" id="qcphone" value="<?php if($postPhone){ echo $postPhone; } else { echo 'Telephone number'; }?>" onBlur="if (this.value == '') {this.value = 'Telephone number';}" onFocus="if (this.value == 'Telephone number') {this.value = '';}" />
                    <input name="qcpage" id="qcpage" value="<?php if(is_single()) { echo 'Blog Post'; } else { echo 'Top Level Blog Sidebar'; }; ?>" type="hidden">
                    <input name="qcpagetitle" id="qcpagetitle" value="<?php if(is_single()) { echo the_title(); } ?>" type="hidden">
                    <input name="qcpageurl" id="qcpageurl" value="<?php if(is_single()) { echo the_permalink(); } ?>" type="hidden">
                    <input type="submit" id="qcsubmit-news" name="submit" value=" SUBMIT " />
                    <p><a href="<?php echo get_permalink(48); ?>">Privacy Policy</a></p>
                </form>
            </div>
        </div>