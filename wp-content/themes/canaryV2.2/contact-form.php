	<div class="contact-form">
        	<div class="contact-form-title"></div>
            <div class="contact-form-body">
            	<p>Contact us to receive a claims pack.</p>
                <?php
				if($badForm){
					echo '<p class="error">Oops! There was an error with your email address!</p>';	
				}
				else if($emailForm){
					echo '<p class="sent">We will respond to your enquiry shortly.</p>';	
				}
				
				if($errormessage){
					echo '<p class="error">'.$errormessage.'</p>';
				}
				?>
                <form name="contactform" id="contactform" action="" method="post" enctype="application/x-www-form-urlencoded">
					<span class="checkbox">
                        <span class="check-wrap conppi">
                                <select name="conppi" id="conppi">
                                	<?php
									$i = 1;
									$a = 9;
									
									while($i <= $a){
										if($i == 1){
											echo '<option value="'.$i.'">'.$i.' PPI Claim</option>';
										}
										else{
											echo '<option value="'.$i.'">'.$i.'</option>';
										}
										$i++;
									}
									/*
									if($i > $a){
										echo '<option value="'.$a.'">'.$a.'+</option>';
										$i++;	
									}
									*/
									?>
                                </select>
                        </span><br />
                        <span class="check-wrap contitle">
                                <select name="contitle" id="contitle">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                </select>
                        </span><br />
                    </span>
                    <input name="conname" id="conname" value="<?php if($postName){ echo $postName; } else { echo 'Name'; }?>" onBlur="if (this.value == '') {this.value = 'Name';}" onFocus="if (this.value == 'Name') {this.value = '';}"/>
                    <?php /*?><input name="conhousenum" id="conhousenum" value="<?php if($postHouseNum){ echo $postHouseNum; } else { echo 'House number'; }?>" onBlur="if (this.value == '') {this.value = 'House number';}" onFocus="if (this.value == 'House number') {this.value = '';}" />
                    <input name="conpostcode" id="conpostcode" value="<?php if($postPostcode){ echo $postPostcode; } else { echo 'Post code'; }?>" onBlur="if (this.value == '') {this.value = 'Post code';}" onFocus="if (this.value == 'Post code') {this.value = '';}" /><?php */?>
                    <input name="conemail" id="conemail" value="<?php if($postEmail){ echo $postEmail; } else { echo 'Email address'; }?>" onBlur="if (this.value == '') {this.value = 'Email address';}" onFocus="if (this.value == 'Email address') {this.value = '';}" />
                    <input name="conphone" id="conphone" value="<?php if($postPhone){ echo $postPhone; } else { echo 'Telephone number'; }?>" onBlur="if (this.value == '') {this.value = 'Telephone number';}" onFocus="if (this.value == 'Telephone number') {this.value = '';}" />
                    <div class="address"><textarea name="conaddress" id="conaddress" cols="" rows="" onBlur="if (this.value == '') {this.value = 'Address';}" onFocus="if (this.value == 'Address') {this.value = '';}" ><?php if($postAdd){ echo $postAdd; } else { echo 'Address'; }?></textarea></div>
                    <input name="conpage" id="conpage" value="Contact Page" type="hidden">
                    <input type="submit" id="consubmit" name="submit" value=" SUBMIT " />
                </form>
            </div>
        </div>