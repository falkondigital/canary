<?php
/*
*
*
*/
$liveHost = true;
if ($_SERVER['HTTP_HOST'] == "localhost") $liveHost = false;
?>

<div class="footer-top"></div>
    <div class="footer">
    	<div class="footer-content">
        <div class="sitemap">
   			<span class="footer-titles">Sitemap</span>
            
            <?php
           // $mypages = get_pages('sort_column=menu_order&sort_order=asc&post_type=page&post_status=publish');
            //foreach($mypages as $page)
           // {						
            ?>
                <?php /*?><a href="<?php echo get_page_link($page->ID) ?>" title="<?php echo $page->post_title ?>" class="footer-sitemap"><?php echo $page->post_title ?></a><br /><?php */?>
            <?php
           // }	
            ?>
            <div class="link1">
            	<a href="<?php bloginfo('url'); ?>" rel="nofollow" title="Home" class="footer-sitemap">Home</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/about-us/"; else echo "?page_id=2" ?>" rel="nofollow" title="About" class="footer-sitemap">About</a><br />
                <?php /*?><a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/credit-card-charges/"; else echo "?page_id=102" ?>" rel="nofollow" title="Credit Card Charges" class="footer-sitemap">Credit Card Charges</a><br /><?php */?>
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/payment-protection/"; else echo "?page_id=5" ?>" rel="nofollow" title="Payment Protection (PPI)" class="footer-sitemap">Payment Protection (PPI)</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/no-win-no-fee/"; else echo "?page_id=6" ?>" rel="nofollow" title="No Win No Fee" class="footer-sitemap">No Win No Fee</a><br />
                <a href="<?php echo get_permalink(382); ?>" rel="nofollow" title="<?php echo get_the_title(382);?>" class="footer-sitemap"><?php echo get_the_title(382);?></a><br />
                <a href="<?php echo get_permalink(447); ?>" rel="nofollow" title="Cookie Policy" class="footer-sitemap">Cookie Policy</a>
            </div>
            <div class="link2">
            	<a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/refer-a-friend/"; else echo "?page_id=7" ?>" rel="nofollow" title="Referrals" class="footer-sitemap">Referrals</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/faqs/"; else echo "?page_id=8" ?>" title="FAQ" rel="nofollow" class="footer-sitemap">FAQ</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/news/"; else echo "?page_id=10" ?>" title="News" rel="nofollow" class="footer-sitemap">News</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/how-it-works/"; else echo "?page_id=3" ?>" rel="nofollow" title="How it Works" class="footer-sitemap">How it Works</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/as-seen-on-tv/"; else echo "?page_id=3" ?>" rel="nofollow" title="As seen on TV" class="footer-sitemap">As seen on TV</a>
            </div>
            <div class="link3">
            	<a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/contact-us/"; else echo "?page_id=9" ?>" rel="nofollow" title="Contact" class="footer-sitemap">Contact</a><br />
                <a href="<?php bloginfo('url'); ?>/category/ppi/" rel="nofollow" title="PPI News" class="footer-sitemap">PPI News</a><br />
                <a href="<?php bloginfo('url'); ?>/category/testimonials/" rel="nofollow" title="Testimonials" class="footer-sitemap">Testimonials</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/privacy/"; else echo "?page_id=348" ?>" rel="nofollow" title="Privacy" class="footer-sitemap">Privacy</a><br />
                <a href="<?php bloginfo('url'); ?><?php if($liveHost) echo "/terms-and-conditions/"; else echo "?page_id=51" ?>" rel="nofollow" title="Terms and conditions" class="footer-sitemap">Terms and conditions</a>
            </div>
        </div>
        <div class="news-signup"><span class="footer-titles">Newsletter sign up</span>
        <span class="newsletter-txt">Keep in touch by subscribing to our mailing list. We will never sell or pass on your information to a third party. Please review our <a href="<?php echo get_permalink(48); ?>" title="Privacy Policy" rel="nofollow" >Privacy Policy</a> before signing up.</span>
        <div class=".newsletter-form">
        <!-- Begin MailChimp Signup Form -->

            <div id="mc_embed_signup">
                <form action="http://canaryclaims.us2.list-manage1.com/subscribe/post?u=af54280acaa5dec64076d719b&amp;id=2f315c3737" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
                    
                    
                <div class="mc-field-group">
                <input type="text" name="FNAME" class="" id="mce-FNAME" onfocus="if(this.value == 'First name'){this.value = '';}" onblur="if(this.value == ''){this.value = 'First name';}" value="First name">
                </div>
                <div class="mc-field-group">
                <input type="text" name="EMAIL" class="required email" id="mce-EMAIL" onfocus="if(this.value == 'Email address'){this.value = '';}" onblur="if(this.value == ''){this.value = 'Email address';}" value="Email address">
                </div>

                <div id="mce-responses">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <div class="nl-submit"><input type="submit" value="sign up" name="subscribe" id="mc-embedded-subscribe" class="btn"></div>
                </form>
            </div>
            
            <?php // if(function_exists('add_footer_snippets')) { add_footer_snippets(); } ?>
            
			<script type="text/javascript">
            var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';
            try {
                var jqueryLoaded=jQuery;
                jqueryLoaded=true;
            } catch(err) {
                var jqueryLoaded=false;
            }
            var head= document.getElementsByTagName('head')[0];
            if (!jqueryLoaded) {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
                head.appendChild(script);
                if (script.readyState && script.onload!==null){
                    script.onreadystatechange= function () {
                          if (this.readyState == 'complete') mce_preload_check();
                    }    
                }
            }
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
            head.appendChild(script);
            var err_style = '';
            try{
                err_style = mc_custom_error_style;
            } catch(e){
                err_style = 'z-index: 1;';
            }
            var head= document.getElementsByTagName('head')[0];
            var style= document.createElement('style');
            style.type= 'text/css';
            if (style.styleSheet) {
              style.styleSheet.cssText = '.mce_inline_error {' + err_style + '}';
            } else {
              style.appendChild(document.createTextNode('.mce_inline_error {' + err_style + '}'));
            }
            head.appendChild(style);
            setTimeout('mce_preload_check();', 250);
            
            var mce_preload_checks = 0;
            function mce_preload_check(){
                if (mce_preload_checks>40) return;
                mce_preload_checks++;
                try {
                    var jqueryLoaded=jQuery;
                } catch(err) {
                    setTimeout('mce_preload_check();', 250);
                    return;
                }
                try {
                    var validatorLoaded=jQuery("#fake-form").validate({});
                } catch(err) {
                    setTimeout('mce_preload_check();', 250);
                    return;
                }
                mce_init_form();
            }
            function mce_init_form(){
                jQuery(document).ready( function() {
                  var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
                  var mce_validator = jQuery("#mc-embedded-subscribe-form").validate(options);
                  jQuery("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
                  options = { url: 'http://canaryclaims.us2.list-manage.com/subscribe/post-json?u=af54280acaa5dec64076d719b&id=2f315c3737&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
                                beforeSubmit: function(){
                                    jQuery('#mce_tmp_error_msg').remove();
                                    jQuery('.datefield','#mc_embed_signup').each(
                                        function(){
                                            var txt = 'filled';
                                            var fields = new Array();
                                            var i = 0;
                                            jQuery(':text', this).each(
                                                function(){
                                                    fields[i] = this;
                                                    i++;
                                                });
                                            jQuery(':hidden', this).each(
                                                function(){
                                                    if (fields.length == 2) fields[2] = {'value':1970};//trick birthdays into having years
                                                    if ( fields[0].value=='MM' && fields[1].value=='DD' && fields[2].value=='YYYY' ){
                                                        this.value = '';
                                                    } else if ( fields[0].value=='' && fields[1].value=='' && fields[2].value=='' ){
                                                        this.value = '';
                                                    } else {
                                                        this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
                                                    }
                                                });
                                        });
                                    return mce_validator.form();
                                }, 
                                success: mce_success_cb
                            };
                  jQuery('#mc-embedded-subscribe-form').ajaxForm(options);      
                  
                });
            }
            function mce_success_cb(resp){
                jQuery('#mce-success-response').hide();
                jQuery('#mce-error-response').hide();
                if (resp.result=="success"){
                    jQuery('#mce-'+resp.result+'-response').show();
                    jQuery('#mce-'+resp.result+'-response').html(resp.msg);
                    jQuery('#mc-embedded-subscribe-form').each(function(){
                        this.reset();
                    });
                } else {
                    var index = -1;
                    var msg;
                    try {
                        var parts = resp.msg.split(' - ',2);
                        if (parts[1]==undefined){
                            msg = resp.msg;
                        } else {
                            i = parseInt(parts[0]);
                            if (i.toString() == parts[0]){
                                index = parts[0];
                                msg = parts[1];
                            } else {
                                index = -1;
                                msg = resp.msg;
                            }
                        }
                    } catch(e){
                        index = -1;
                        msg = resp.msg;
                    }
                    try{
                        if (index== -1){
                            jQuery('#mce-'+resp.result+'-response').show();
                            jQuery('#mce-'+resp.result+'-response').html(msg);            
                        } else {
                            err_id = 'mce_tmp_error_msg';
                            html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
                            
                            var input_id = '#mc_embed_signup';
                            var f = jQuery(input_id);
                            if (ftypes[index]=='address'){
                                input_id = '#mce-'+fnames[index]+'-addr1';
                                f = jQuery(input_id).parent().parent().get(0);
                            } else if (ftypes[index]=='date'){
                                input_id = '#mce-'+fnames[index]+'-month';
                                f = jQuery(input_id).parent().parent().get(0);
                            } else {
                                input_id = '#mce-'+fnames[index];
                                f = jQuery().parent(input_id).get(0);
                            }
                            if (f){
                                jQuery(f).append(html);
                                jQuery(input_id).focus();
                            } else {
                                jQuery('#mce-'+resp.result+'-response').show();
                                jQuery('#mce-'+resp.result+'-response').html(msg);
                            }
                        }
                    } catch(e){
                        jQuery('#mce-'+resp.result+'-response').show();
                        jQuery('#mce-'+resp.result+'-response').html(msg);
                    }
                }
            }
			
			/*jQuery('li.recent-list > div').hide();*/
			
			jQuery('li.recent-list > p').click(function() { 
			jQuery(this).toggleClass("active").next().slideToggle('slow');
			jQuery('li.recent-list > div').show();
				return false; //Prevent the browser jump to the link anchor
			}); 
			
			jQuery('li.cats-list > div').hide();
			
			jQuery('li.cats-list > p').click(function() { 
			jQuery(this).toggleClass("active").next().slideToggle('slow');
			jQuery('li.cats-list > div').show();
				return false; //Prevent the browser jump to the link anchor
			}); 
			
			jQuery('li.arc-list > div').hide();
			
			jQuery('li.arc-list > p').click(function() { 
			jQuery(this).toggleClass("active").next().slideToggle('slow');
			jQuery('li.arc-list > div').show();
				return false; //Prevent the browser jump to the link anchor
			}); 
            
            </script>
            
            <!--End mc_embed_signup-->
        </div>
        </div>
        <div class="footer-blurb">
        <span class="blurb-txt">Claims Thru Us Limited trading as Canary Claims is regulated by the Ministry of Justice in respect of regulated claims<br />management activities; its registration is recorded on the website: <a href="http://www.claimsregulation.gov.uk" title="www.claimsregulation.gov.uk" class="blurb-link" rel="nofollow" target="_blank">www.claimsregulation.gov.uk</a>
		<br  /><strong>Authorisation Number: CRM2233</strong> &copy; Canary Claims. All Rights Reserved <?php echo date('Y'); ?></span>
        </div>
        <?php /*?><div class="ico-logo"><img src="<?php bloginfo('url'); ?>/assets/logo-ico.jpg" title="Information Commissioner's Office" alt="Information Commissioner's Office Logo" /></div><?php */?>
        </div>
    </div>
    
    
</div> <!--MAIN -->
