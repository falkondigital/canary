<?php
/** Template Name: Mis Sold PPI page
**/
include('quickclaim.php'); 
get_header(); ?>

<body>
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="post" id="post-<?php the_ID(); ?>" style="width:540px;float:left;">
                <div class="entry">
                    <?php the_content(); ?>
                </div>
            </div>
            <?php endwhile; endif; ?>
            <div style="float:right;margin-top:50px;">
            	<?php include('quickclaim-full.php'); ?>
            </div>
            <div style="clear:left"></div>
			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
            
		</div>
		<div class="content-bottom"></div>
	</div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
</body>

</html>
