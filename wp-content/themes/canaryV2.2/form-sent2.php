<?php
/**
 * @package WordPress
 * @subpackage Canary Claims
 * Template Name: Form Sent New Page
 */

get_header(); ?>
<SCRIPT language="JavaScript" type="text/javascript">
<!-- Yahoo!
window.ysm_customData = new Object();
window.ysm_customData.conversion = "transId=,currency=,amount=";
var ysm_accountid  = "109LGJ0B2K43737CCURHPR026U8";
document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
+ "SRC=//" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
+ "></SCR" + "IPT>");
// -->
</SCRIPT>
<body id="contact">
	<?php 
	  require('can-navigation.php');
	?>

     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        	<div>
            	<h1>Thank you</h1>
                
                
                <div class="col1">
                	<div class="ty-bg">
                  		<p style="margin-left: -5px; padding-right:14px;">
                        	Thank you for your enquiry. An application pack will be sent to you today.<br /><br />

                            Please do not hesitate to contact us if you have any queries once you have received the forms.
                        </p>
                	</div>                  
                </div>
                
                
                <div class="col2">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="post" id="post-<?php the_ID(); ?>">
                    <div class="entry">
                        <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            
                        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                            </div>
                        </div>
                        <div class="contact-downloads"><p><strong><span style="color:#4b8a35;">Download Application Forms:</span></strong></p>
                        <p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/PPI_claim.pdf" title="PPI_claim.pdf">Application form 1 - Payment Protection Insurance</a></p>
                        <?php /*?><p class="pdfdown"><a href="<?php bloginfo('url'); ?>/pdf/Credit_Card_Charge_Claim.pdf" title="Credit_Card_Charge_Claim.pdf">Application form 2 - Credit Card Charges</a></p><?php */?>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
                <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
                </div>
			</div>
            <div class="content-bottom"></div>
        </div>
        			
	<?php 
		require('can-footer.php'); 
	?>
    
<!-- Google Code for Form Enquiry Conversion Page -->
<script type="text/javascript">
<!--
var google_conversion_id = 1040779051;
var google_conversion_language = "en_GB";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "ipvBCLP5RhCrjqTwAw";
var google_conversion_value = 0;
//-->
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1040779051/?label=ipvBCLP5RhCrjqTwAw&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<SCRIPT>
microsoft_adcenterconversion_domainid = 46945;
 microsoft_adcenterconversion_cp = 5050;
 </script>
<SCRIPT SRC="http://0.r.msn.com/scripts/microsoft_adcenterconversion.js"></SCRIPT>
</body>

</html>