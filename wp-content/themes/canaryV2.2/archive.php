<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */
include('quickclaim.php');
get_header(); ?>
<body id="news">
	<?php 
	  require('can-navigation.php');
	?>
    
     <div class="content">
    	<div class="content-top"></div>
        <div class="content-middle">
        
           <div class="content-left-blog">
                <?php get_sidebar(); ?>
           </div>
           
           <div class="content-right-blog">

<?php
	/* Queue the first post, that way we know
	 * what date we're dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		the_post();
?>

			<h1>
<?php if ( is_day() ) : ?>
				<?php printf( __( 'Daily Archives: %s', 'twentyten' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( 'Monthly Archives: %s', 'twentyten' ), get_the_date('F Y') ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( 'Yearly Archives: %s', 'twentyten' ), get_the_date('Y') ); ?>
<?php else : ?>
				<?php _e( 'Blog Archives', 'twentyten' ); ?>
<?php endif; ?>
			</h1>

<?php
	/* Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/* Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archives.php and that will be used instead.
	 */
	 //get_template_part( 'loop', 'archive' );

if (have_posts()) :
    while (have_posts()) : the_post();?>
    <div class="blog-summary-wrapper">
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        
         
 
        <div class="entry-sum">
        <p class="blogheading"><a href="<?php the_permalink() ?>" class="blogheading" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
        <?php the_excerpt(); ?>
        </div>
		
        <p class="postmetadata-right">Posted in <?php the_category(', ') ?> | By <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="nofollow">
									<?php printf( __('%s'),get_the_author());?></a> | <?php the_time('F jS, Y') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></p>
                                    
       
       
        <br />
    </div>
    </div>
    <div class="divide-horizontal"></div>
	<?php endwhile; ?>
		
    <div class="blognavigation">
        	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>
</div>
	
	 </div>
     <div class="content-bottom"></div>    			
	<?php 
		require('can-footer.php'); 
	?>
    
			
</body>

</html>