

<?php
/**
 * @package WordPress
 * @subpackage Consult Retail Theme
 * Template Name: Webform
 */
require('wp-blog-header.php');

get_header(); ?>

<body>
	<?php 
	  require('wp-content/themes/consultretail/cr-navigation.php');
	?>
      <div class="content-holder">
            <div class="content-left">
                <img id="sidebar" src="http://www.consultretail.co.uk/assets/sidebar-registration.jpg" alt="A student reading in the library" />
            </div>
            <div class="content-right">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="post" id="50">
                <div class="entry">
                    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        
                    <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
        
                </div>
            </div>
            <?php endwhile; endif; ?>
            
<?php

error_reporting(0);

$reg_title = $_POST['reg_title'];
$reg_first_name = $_POST['reg_first_name'];
$reg_surname = $_POST['reg_surname'];
$reg_job_title = $_POST['reg_job_title'];
$reg_department = $_POST['reg_department'];
$reg_uc = $_POST['reg_uc'];
$reg_email_address = $_POST['reg_email_address'];
$reg_telephone_number = $_POST['reg_telephone_number'];
$reg_1 = $_POST['reg_1'];
$reg_2 = $_POST['reg_2'];
$reg_3 = $_POST['reg_3'];
$reg_4 = $_POST['reg_4'];
$reg_5 = $_POST['reg_5'];
$reg_6 = $_POST['reg_6'];
$reg_7 = $_POST['reg_7'];
$reg_8 = $_POST['reg_8'];
$reg_9 = $_POST['reg_9'];
$reg_10 = $_POST['reg_10'];
$reg_11 = $_POST['reg_11'];
$reg_12 = $_POST['reg_12'];
$reg_13 = $_POST['reg_13'];
$reg_not_by_email = $_POST['reg_not_by_email'];
$reg_not_by_telephone = $_POST['reg_not_by_telephone'];


/* first check to make sure required fields have all been filled in */

$fields = array('firstname' => $reg_first_name, 'surname' => $reg_surname, 'email' => $reg_email_address);
$errors = false;

foreach($fields as $key => $val){
	if(empty($val)){
		$errors = true;		 
	}
}


if($errors == false){

	/* code to replace null values in the checklist with "no" */
	$i=1;
	while($i<=13){
		$temp = ${"reg_{$i}"};
		if($temp == ''){
			${"reg_{$i}"} = 'No';
		}
		$i++;
	}
	
	if($reg_not_by_email == '')$reg_not_by_email = 'No';
	if($reg_not_by_telephone == '')$reg_not_by_telephone = 'No';
	
	
	$body_content = "Title:&nbsp;&nbsp;&nbsp;".$reg_title." \n ";
	$body_content .= "First name:&nbsp;&nbsp;&nbsp;".$reg_first_name." \n "; 
	$body_content .= "Surname:&nbsp;&nbsp;&nbsp;" . $reg_surname . "\n";
	$body_content .= "Job title:&nbsp;&nbsp;&nbsp;" . $reg_job_title . "\n";
	$body_content .= "Dept:&nbsp;&nbsp;&nbsp;" . $reg_department . "\n";
	$body_content .= "University:&nbsp;&nbsp;&nbsp;" . $reg_uc . "\n";
	$body_content .= "Email:&nbsp;&nbsp;&nbsp;" . $reg_email_address . "\n";
	$body_content .= "Telephone:&nbsp;&nbsp;&nbsp;" . $reg_telephone_number . "\n";
	$body_content .= "\n ------------ \n \n";
	$body_content .= "Student research:&nbsp;&nbsp;&nbsp;" . $reg_1 . "\n";
	$body_content .= "General information:&nbsp;&nbsp;&nbsp;" . $reg_2 . "\n";
	$body_content .= "Retail tenants:&nbsp;&nbsp;&nbsp;" . $reg_3 . "\n";
	$body_content .= "Catering tenants:&nbsp;&nbsp;&nbsp;" . $reg_4 . "\n";
	$body_content .= "Retail consultants:&nbsp;&nbsp;&nbsp;" . $reg_5 . "\n";
	$body_content .= "Catering consultants:&nbsp;&nbsp;&nbsp;" . $reg_6 . "\n";
	$body_content .= "Research companies:&nbsp;&nbsp;&nbsp;" . $reg_7 . "\n";
	$body_content .= "Shop design:&nbsp;&nbsp;&nbsp;" . $reg_8 . "\n";
	$body_content .= "EPOS providers:&nbsp;&nbsp;&nbsp;" . $reg_9 . "\n";
	$body_content .= "Property agents:&nbsp;&nbsp;&nbsp;" . $reg_10 . "\n";
	$body_content .= "Retail training providers:&nbsp;&nbsp;&nbsp;" . $reg_11 . "\n";
	$body_content .= "Branded merchandise:&nbsp;&nbsp;&nbsp;" . $reg_12 . "\n";
	$body_content .= "Student product packs:&nbsp;&nbsp;&nbsp;" . $reg_13 . "\n";
	$body_content .= "\n ------------ \n \n";
	$body_content .= "Information via email:&nbsp;&nbsp;&nbsp;" . $reg_not_by_email . "\n";
	$body_content .= "Contact via phone:&nbsp;&nbsp;&nbsp;" . $reg_not_by_telephone . "\n";
	
	/* send via email */
	?>
    
    
    <p><?php echo nl2br($body_content) ?></p>
	
    <?php
	
	/*
	// Contact subject
	$subject ="Online form";
	// Details
	$message="$reg_title";
	
	// Mail of sender
	$mail_from="$customer_mail";
	// From
	$header="from: $name <$mail_from>";
	
	// Enter your email address
	$to ='someone@somewhere.com';
	
	$send_contact=mail($to,$subject,$message,$header);
	
	// Check, if message sent to your email
	// display message "We've recived your information"
	if($send_contact){
	echo "We've recived your contact information";
	}
	else {
	echo "ERROR";
	}*/

}
else {
	/* Error message */
	?>
    
    <p>You haven't filled in all the required fields!</p>
        
    <?php
}
?>

</div>
	
	 </div>
        			
	<?php 
		require('cr-footer.php'); 
	?>
    
			
</body>

</html>