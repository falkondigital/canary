/***********************************
*   http://javascripts.vbarsan.com/
*   This notice may not be removed
***********************************/

//-- Begin Scroller's Parameters and message -->
//scroller width: change to your own;
var swidth=340;

//scroller height: change to your own;
var sheight=175;

//background color: change to your own;
var sbcolor="none";

//scroller's speed: change to your own;
var sspeed=2;
var restart=sspeed;
rspeed=sspeed;


//text: change to your own
wholemessage ='<p>"I just wanted to thank you so much for the successful proof of mis-sale of PPI on the Virgin/MBNA card for me.  I was thrilled to be told I was going to receive a cheque, which arrived today!"<br /><span>Mrs P Tremain, 29 July 2011<br>PPI refunded: £492.51</span></p>';

wholemessage +='<p>"I would like to say thank you and more power to your company... God bless."<br /><span>Mrs E. G. Coronel, July 2011<br>PPI refunded: £2,947</span></p>';

wholemessage +='<p>"I wish to convey my thanks for your help in this matter."<br /><span>Miss C Jones, July 2011<br>PPI refunded: £2,650</span></p>';

wholemessage +='<p>"A big thank you for all your help."<br /><span>Mr & Mrs Gardner, 2 July 2011<br>PPI refunded: £6,505</span></p>';

wholemessage +='<p>"I\'m very grateful for the work you have put in with relation to my two card claims and would recommend your company to others."<br /><span>Mrs Pauline Tremain, 28 March 2011</span></p>';

wholemessage +='<p>"Thanks for your company\'s efforts - I will recommend you to all."<br /><span>Mr Peter J Gavaghan, 14 February 2011</span></p>';

wholemessage +='<p>"I would like to take this opportunity to thank you for the excellent service you have provided regarding this matter."<br /><span>Mr Philip E Hutchinson, 29 December 2010<br>PPI reclaimed: £2,050</span></p>';

wholemessage+='<p>"I certainly didn\'t feel pestered by yourselves, the whole process has been very simple and easy and it\'s much appreciated and many thanks."<br /><span>Mr & Mrs D Roberts</span></p>';

wholemessage+='<p>"We would like to thank you for your great help."<br /><span>Mr & Mrs Capuyan, 11 January 2011<br>Total compensation: £6,400</span></p>';

wholemessage+='<p>"I was so pleased with the way your company dealt with my claim that I recommended your services to my husband."<br /><span>Mrs Barbara Gibson, 19 December 2010<br>Total Compensation: £6,063</span></p>';

wholemessage+='<p>"I must thank you for your assistance in winning this claim for me. I have recommended your company to my colleagues."<br /><span>Mr A G Dixey, 25 November 2010<br>Total Compensation: £9,506.64</span></p>';

wholemessage+='<p>"I would like to thank you very much for helping me reclaim my mis-sold PPI."<br /><span>Miss C Lewis, 30 September 2010<br>Total Compensation: £4,185.04</span></p>';

wholemessage+='<p>"I would like to thank you for your help, God bless you! I will recommend you to all my friends."<br /><span>Mrs Felma Patio, 27 September 2010</span></p>';

wholemessage+='<p>"I would like to thank you for your help for the insurance claim. It is greatly appreciated."<br /><span>Mr Daniel Flosa, 31 August 2010</span></p>';

wholemessage+='<p>"We would like to also take this opportunity to thank you for your time and assistance in dealing with our matter."<br /><span>Miss K Major, 23 August 2010<br>Compensation: £9,887.25</span></p>';

wholemessage+='<p>"Thanks once again for dealing with my claim, I would not have done it without your help."<br /><span>Miss A Khatun, 7 August 2010</span></p>';

wholemessage+='<p>"Thank you so much for your swift and efficient service. I am extremely pleased with the settlement..."<br /><span>Mrs J Bellis, 2 June 2010</span></p>';

wholemessage+='<p>"I would like to say your company has provided a great service and has been very efficient and have recommended you to my friends."<br /><span>Miss Claire Dawson, 15 May 2010</span></p>';

wholemessage+='<p>"I would like to say a quick thank you for the speedy way you have treated my claim, I will certainly be passing your details to my friends and family."<br /><span>Mr Roger Jenks, 27 May 2009</span></p>';

wholemessage+='<p>"Thank you for your prompt and successful assistance."<br /><span>Yours gratefully, Mr P Plucknett - May 2009</span></p>';

wholemessage+='<p>"Thank you so much for settling our claim with AXA. It is very much appreciated.........We would certainly recommend you to others."<br /><span>Mr & Mrs Winfield - 1 April 2009</span></p>';

wholemessage+='<p>""...Thanks for your help and support."<br /><span>Mr Neil Chilcott - 31 March 2009</span></p>';

wholemessage+='<p>"Dear sir,<br />I thought that I must take some time to write, to thank you for the service that you are currently providing to resolve my complaints with three different companies. <br /><br />It is regrettably an all too familiar story these days that the public seem to receive poor levels of customer care when dealing with financial institutions but I am happy to note that your company has dealt with my complaints against the providers as quickly and efficiently as possible and have kept me informed at each stage in writing of the progress of each of the complaints.<br /><br />I am so pleased with your service that I have recommended your company to two of my friends and my Brother who after contacting yourselves received paperwork to initiate a claim within days.<br /><br />Working within the service industry myself, I know that praise is very rare but credit where it is due, well done to all of your team for a job well done making a rather protracted process as easy as possible."<br /><br />Best regards <br /><span>R P Holden</span></p>';

wholemessage+='<p>"...Thank you very much for your help in achieving a satisfactory outcome."<br /><span>Mr & Mrs A Brown - Oct/2008</span></p>';

wholemessage+='<p>"Thank you for your assistance in this matter, we are very pleased with the outcome."<br /><span>Mr Welford & Ms Wallace</span></p>';

wholemessage+='<p>"Once again I would like to thank you for the speedy and efficient way you dealt with this claim."<br /><span>Mr & Mrs E Anderson</span></p>';

wholemessage+='<p>"...Thank you for a prompt service."<br /><span>Mrs J R Glynn</span></p>';

wholemessage+='<p>"May I thank you from my wife & I for all your help in getting our compensation, we are very grateful and by way of thanks we have forwarded your company name to many of our friends and family. Cheers!"<br /><span>Mr D Lane</span></p>';

wholemessage+='<p>"Thank you very much for your work and would just like to say how pleased I was with the result, and I have passed your company name & number to family and friends."<br /><span>Mr & Mrs Dowson</span></p>';

wholemessage+='<p>"I would like to take this opportunity to thank you for all the hard work that you have put in to get us the compensation that we have received."<br /><span>Phil & Sue Webb</span></p>';

wholemessage+='<p>"... I received a compensation offer from Lloyds TSB regarding my complaint which you have taken up on my behalf ... I would also like to thank you for a professional service."<br /><span>Dirk Lee</span></p>';

wholemessage+='<p>"Thank you for a very professional service."<br /><span>Mr G Lloyd</span></p>';

wholemessage+='<p>"...I was advised to contact you by a relative who works for an insurance company, as you have an excellent record for successful claims and are an awful lot cheaper than others."<br /><span>Mr S Kimberley</span></p>';



//-- end Parameters and message -->

//-- begin: Scroller's Algorithm -->
function goup(){if(sspeed!=rspeed*8){sspeed=sspeed*2;restart=sspeed;}}
function godown(){if(sspeed>rspeed){sspeed=sspeed/2;restart=sspeed;}}
function start(){if(document.getElementById)ns6marquee(document.getElementById('slider'));else if(document.all)iemarquee(slider);else if(document.layers)ns4marquee(document.slider1.document.slider2);}function iemarquee(whichdiv){iediv=eval(whichdiv);iediv.style.pixelTop=sheight+"px";iediv.innerHTML=wholemessage;sizeup=iediv.offsetHeight;ieslide();}function ieslide(){if(iediv.style.pixelTop>=sizeup*(-1)){iediv.style.pixelTop-=sspeed+"px";setTimeout("ieslide()",100);}else{iediv.style.pixelTop=sheight+"px";ieslide();}}function ns4marquee(whichlayer){ns4layer=eval(whichlayer);ns4layer.top=sheight;ns4layer.document.write(wholemessage);ns4layer.document.close();sizeup=ns4layer.document.height;ns4slide();}function ns4slide(){if(ns4layer.top>=sizeup*(-1)){ns4layer.top-=sspeed;setTimeout("ns4slide()",100);}else{ns4layer.top=sheight;ns4slide();}}function ns6marquee(whichdiv){ns6div=eval(whichdiv);ns6div.style.top=sheight+"px";ns6div.innerHTML=wholemessage;sizeup=ns6div.offsetHeight;ns6slide();}function ns6slide(){if(parseInt(ns6div.style.top)>=sizeup*(-1)){ns6div.style.top=parseInt(ns6div.style.top)-sspeed+"px";setTimeout("ns6slide()",100);}else{ns6div.style.top=sheight+"px";ns6slide();}}
//-- end Algorithm -->
